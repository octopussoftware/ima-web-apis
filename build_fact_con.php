<?php
include('./connect.php');
include('./funciones_gen.php');
$id_convenio = $_POST['id_conv'];
$id_inm = $_POST['id_inm'];
$id_cuota = $_POST['id_cuota'];
$ag_ret = $_POST['ag_ret']; 

$sql="select * from x_convenio where id = $id_cuota";
$result = $conn->query($sql);
$xconv = $result->fetch_assoc();


$curr = date('Y-m-d');

$monto_cuota = $xconv['monto_cuota'];
$fecha_vence=$xconv['fecha_vence'];

$total_fact = number_format($monto_cuota*1.12,2,'.','');
$monto_sin_iva = number_format($monto_cuota,2,'.','');
$iva_fact = number_format($monto_cuota*0.12,2,'.','');

$sql="select * from pagos where tipo_pago='conv' and id_doc=$id_cuota";  //se verifica que la factura se creo
$result = $conn->query($sql);
$row_pag = $result->fetch_assoc();


if (count($row_pag) == 0) {
$multa = 0;
     if ($curr > $fecha_vence) {
        $multa = 30*300;
     }

$total_fact_multa = number_format($total_fact + $multa,2,'.','');

$sql="insert into pagos (id_inm,monto,monto_sin_iva,iva,estatus,tipo_pago,id_doc,monto_multa) values ($id_inm,$total_fact_multa,$monto_sin_iva,$iva_fact,'nuevo','conv',$id_cuota,$multa)";


$conn->query($sql);

$sql = "SELECT LAST_INSERT_ID()";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$id_pagos = $row['LAST_INSERT_ID()'];

$sql="insert into d_pagos (id_pagos,periodo,descripcion,monto) values ($id_pagos,'convenio','monto_cuota',$total_fact)";
$conn->query($sql);

if ($multa != 0) {
$sql="insert into d_pagos (id_pagos,periodo,descripcion,monto) values ($id_pagos,'convenio','multa',$multa)";
$conn->query($sql);
}

} else {

$id_pagos = $row_pag['id'];
$multa = $row_pag['monto_multa'];

}

if ($row_pag['nro_control'] == '') {
$sql="update pagos set estatus = 'inf_fact' where id =$id_pagos";
$conn->query($sql); 
}

//verificar cuando se llama sin enviar id_pagos
$sql="select p.id,c.razon,c.ci_rif,c.dir,c.telf,p.fecha_fact,nro_factura,i.id id_inm,p.monto from pagos p 
inner join inmuebles i on (i.id = p.id_inm)
inner join contribuyente c on (c.id = i.id_contribuyente)
where p.id = $id_pagos";

$result = $conn->query($sql);
$header = $result->fetch_assoc();  

$sql="select i.id,tar.descripcion,parr.parr,z.zona,i.av_calle, e.edif, i.no_inmueble, i.piso, i.mts from inmuebles i 
inner join parroquias parr on (parr.id = i.id_parroquia)
inner join zonas z on (z.id = i.id_zona)
inner join edif e on (e.id = i.id_edif)
left join tarifas_com tar on (tar.id = i.id_actividad)
where i.id = $id_inm";//direccion de inmueble

$result = $conn->query($sql);
$inmdir = $result->fetch_assoc();

if ($inmdir['descripcion'] == '') {
	$tipo = "Residencial, Mts.: ".$inmdir['mts'];
} else {
	$tipo = "Comercial / ".$inmdir['descripcion'].", Mts.: ".$inmdir['mts'];
}

$av_calle=$inmdir['av_calle'];
$no_inmueble=$inmdir['no_inmueble'];
$piso=$inmdir['piso'];

$dir_ubi = "Parroquia ".$inmdir['parr'].", Sector ".$inmdir['zona'].", ".$inmdir['edif'];

if (!is_null($av_calle)) $dir_ubi=$dir_ubi.", Av / Calle $av_calle";
if (!is_null($no_inmueble)) $dir_ubi=$dir_ubi.", $no_inmueble";
if (!is_null($piso)) $dir_ubi=$dir_ubi.", Piso $piso";

$fechafact = date("d-m-Y", strtotime($header['fecha_fact']));

$sql="select count(*) as cuantos from documentos where id_pagos = $id_pagos";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();

if ($row['cuantos'] == 0) {
$referencia = generardoc('fac'); 
$nro_control = generardoc('nro_ctrl_web');  
$curr = date('Y-m-d H:i:s', time());
if ($multa == 0) {
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('fac','$referencia',$id_pagos,'$curr','$curr','$curr','$nro_control','$total_fact')";
$conn->query($sql);
  } else {
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('fac','$referencia',$id_pagos,'$curr','$curr','$curr','$nro_control','$total_fact')";
$conn->query($sql);
$referencia_rec = generardoc('rec');
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('rec','$referencia_rec',$id_pagos,'$curr','$curr','$curr','N/A','$multa')";
$conn->query($sql);
}
} else {
$sql="select * from documentos where id_pagos = $id_pagos";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();
$referencia = $row['referencia'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>FACTURA DE PAGO CONVENIO IMA</title>
<!-- <link href="assets/css/fact-styles/factura.css" rel="stylesheet" type="text/css" /> -->
</head>
<body onload="window.print()">
<div class="contenerdorFactura">
	<div class="cuadroTop">
    	<div class="cuadroTopDer">
        	<div class="lineTop"><b>Razón Social/Nombre: </b><? echo $header['razon']?></div>
            <div class="lineTop">
            	<b>RIF / CI: </b><? echo $header['ci_rif']?>                            </div>
            <div class="lineTop">
            	<b>Dirección Fiscal: </b><? echo $header['dir']?>
                <span id="dirfac"></span>
            </div>
            <div class="lineTop">
            	<b>Tipo de Inmueble: </b><? echo $tipo?></br>
            	<b>Datos del Inmueble: </b><? echo $dir_ubi?>
            	<span id="dirfac"></span>
            </div>
        </div>
        <div class="cuadroTopIzq">
        	<div class="lineTop"><b>N° Factura: <? echo $referencia?><br> 
        	Serie </b></div>
            <div class="lineTop"><b>Fecha de Emisión: </b><? echo $curr?></div>
                        <div class="lineTop"><b>Usuario: </b>Cajero Web </b></div>
                    </div>
    </div>
    <div class="cuadroCenter">
    	<div class="lineCemter">
        	<div class="item">
        	  <div align="center"><B>CONCEPTO</B></div></div>
            <div class="monto">
              <div align="center"><B>TOTAL</B></div></div>
        </div>    
        <div class="lineSeparador"></div>
                <div class="lineCemter">
        	<div class="item">
            </br>
            <div align="left">Cuota Nro. <? echo $xconv['cuota']?> correspondiente al convenio <? echo $xconv['id_convenio']?>, con vencimiento el <? echo $xconv['fecha_vence']?> </div>
            </div>
            </br>
            <div class="monto" align="right"><div align="right"><? echo number_format($monto_sin_iva,2,',','.')?></div></div>
        </div> 
    </div>
    <div class="lineSeparador"></div>
    <div class="cuadroFooter">
    <div class="cuadroFormas">
    			<div>Este servicio está exento del 2% del ISLR </div>
              <div id="forma"><b>Formas de pago:</b></div>
                </div>
    	<div class="cuadroTotales">
        	<div class="lineTotales">
            	<div class="totalesIzq"><b>SUB-TOTAL  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($monto_sin_iva,2,',','.')?></div></div>            	
            </div>
            <div class="lineTotales">
            	<div class="totalesIzq"><b>IVA 12%  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($iva_fact,2,',','.')?></div></div>             	
            </div>
            <div class="lineTotales">
            	<div class="totalesIzq"><b>TOTAL  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($total_fact,2,',','.')?></div></div>            	
            </div>
        </div>    
    </div>
</div>
</body>  

<style>
    @charset "utf-8";
/* CSS Document */
body{
    font-family:"Segoe UI";
}
.contenerdorFactura{
    width:900px;
    overflow:hidden;
    margin-top:100px;
}
.contenerdorFactura2{
    width:900px;
    overflow:hidden;
    margin-top:0px;
}
.contenerdorFactura3{
    width:900px;
    overflow:hidden;
    margin-top:180px;
}
.contenerdorFactura2{
    width:900px;
    overflow:hidden;
}
.cuadroTop{
    width:100%;
    overflow:hidden;
}
.cuadroTopDer{
    height:130px;
    width:65%;
    float:left;
}
.cuadroTopIzq{
    height:80px;
    width:33%;
    float:left;
    padding-left:10px;
    padding-top:40px;
    
}
.lineTop{
    width:100%;
    height:20px auto;
}
.cuadroCenter{
    width:100%;
    margin-top:10px;
    height:200px;
}
.cuadroCenterEstado{
    width:100%;
    margin-top:10px;
    height: auto ;
}
.lineCemter{    
    width:100%;
    height:20px;
}
.item{
    width:91%;
    float:left;
}
.monto{
    width:6%;
    float:left;
    margin-left:1%
}
.lineSeparador{
    width:100%;
    border-top:2px #000000 solid;
    margin-top:5px;
}
.lineSeparador2{
    width:100%;
    border-top:2px #000000 solid;
    margin-top:5px;
    overflow:hidden;
}
.cuadroFooter{
    width:100%;
    overflow:hidden;
}
.cuadroTotales{
    width:30%;
    float:right;
}
.cuadroFormas{
    width:68%;
    float:left;
}
.lineTotales{
    width:100%;
    border:#FF0;
    overflow:hidden;
}
.totalesIzq{
    width:47%;
    padding-right:5px;
    float:left;
    text-align: right;
}
.totalesDer{
    width:47%;
    padding-left:5px;
    float:left;
    text-align: left;
}
.tituloEstado{
    border-top:1px #000000 solid;
    border-bottom:1px #000000 solid;
    text-align:center;
    font-size:18px;
}
#forma{
    width:auto;
    float:left;
    margin-right:3px;
    font-size:12px;
}
#dirfac{
    font-size:12px;
}

.centro{
    font-size: 12px;
    text-align: left;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}

.menbrete{
    background-color:#3BFF17;
    color:#FFFFFF;
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}
.menbrete2{
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}

.imgmenbrete{
    float:right;
    z-index:2;
}

.floor{
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:9px;
}


</style>                    

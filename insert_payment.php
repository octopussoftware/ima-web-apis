<?php
include('./connect.php');
include('./funciones_gen.php');

$id_pago = $_POST['id_pago'];
$id_tipo_pago = $_POST['id_tipo_pago'];

if (isset($_POST['ag_ret'])) $ag_ret = $_POST['ag_ret'];

$monto = $_POST['amount'];

if ($monto == 'ret') {

    $ag_ret = 1;

    $sql="select monto from d_pagos where descripcion = 'ret_iva'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

    $monto = $row['monto'];
} 

$id_cajero = $_POST['id_cajero'];

$fecha_reg = date("Y/m/d");
$fecha_mod = date("Y/m/d");

//Si el cajero es 5 (Cajero Web) y tipo pago es 5/6 es un reporte de pago
if($id_cajero == 5 && ($id_tipo_pago == 5 || $id_tipo_pago == 6))
    $estatus = 'reportado';
else
    $estatus = 'aprobado';

if (isset($_POST['com'])) {
    $comentario = $_POST['com'];
} else {
    $comentario = '';
}


$sql="select count(*) as cuantos from x_pagos where id_pagos = $id_pago";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$cuantos = $row['cuantos'];

if ($cuantos == 0) {

$sql="select monto from pagos where id = $id_pago";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$pagado = 0;
$monto_total = $row['monto'];

} else {

// $sql="select sum(x.monto) as pagado,p.monto from x_pagos x
// inner join pagos p on (p.id = x.id_pagos)
// where x.id_pagos = $id_pago and x.id_tipo_pago != 75 group by p.monto";
$sql="select sum(x.monto) as pagado,p.monto from x_pagos x
inner join pagos p on (p.id = x.id_pagos)
where x.id_pagos = $id_pago group by p.monto";
// echo $sql;
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$pagado = $row['pagado'];
$monto_total = $row['monto'];

}

$faltan = number_format($monto_total - $pagado, 2, '.', '');  


if ($monto > $faltan ) {
// if (true) {    
//ya esta pago o el monto es mayor al faltante
    $resp["error"] = true;
    $resp["data"] = 'amount_higher_than_saldo';
    //$resp['data'] = "pagado --- $pagado --- monto_total --- $monto_total --- faltan --- $faltan --- monto --- $monto";
    echo json_encode($resp);
    exit;
}


// echo json_encode("pagado --- $pagado --- monto_total --- $monto_total --- faltan --- $faltan --- monto --- $monto");
    // exit;


$saldo = (float)($faltan - $monto);
// echo $saldo.PHP_EOL;
// exit;

switch ($id_tipo_pago)
{
	case '1':   //efectivo
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario,saldo) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', $saldo)";
    break;

    
    case '2':   //cheque
    $referencia = $_POST['referencia'];
    $id_banco_out = $_POST['id_banco_out'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_out,saldo,fecha_doc) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_out',$saldo,'$fecha_doc')";
    break;
 
    case '3':   //debito
    $referencia = $_POST['referencia'];
    $id_banco_out = $_POST['id_banco_out'];
    $id_banco_in = $_POST['id_banco_in'];
    $lote = $_POST['lote'];
    $aprobacion = $_POST['aprobacion'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_out,id_banco_in,lote,aprobacion,saldo) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_out', '$id_banco_in', '$lote', '$aprobacion',$saldo)";
    break;

    case '4':   //credito
    $referencia = $_POST['referencia'];
    $aprobacion = $_POST['aprobacion'];
    $origen = $_POST['origen'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia,aprobacion,saldo,origen) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$aprobacion',$saldo, '$origen')";
    break;

    case '5' :   //transferencia 
    $referencia = $_POST['referencia'];
    $id_banco_out = $_POST['id_banco_out'];
    $id_banco_in = $_POST['id_banco_in'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_out,id_banco_in,saldo,fecha_doc) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_out', '$id_banco_in', $saldo,'$fecha_doc')";
    break;

    case '6' :   //deposito
    $referencia = $_POST['referencia'];
    $id_banco_in = $_POST['id_banco_in'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_in,saldo,fecha_doc) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_in', $saldo,'$fecha_doc')";
    break;

    case '75' :   //retencion
    $referencia = $_POST['referencia'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, saldo,fecha_doc) values ($id_pago, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', $saldo,'$fecha_doc')";
    break;

	
}


// echo $sql.PHP_EOL;

if ($conn->query($sql) === TRUE) {

$sql = "SELECT LAST_INSERT_ID()";
$result = $conn->query($sql);
$row = $result->fetch_assoc();


$estatus = 'proceso';

if ($saldo == 0 && $ag_ret == 0) {
    if(chequearReportePagoWeb($id_pago) == 0)
        $estatus = 'imp_factura';
    else
        $estatus = 'reportado';
}

if ($saldo == 0 && $ag_ret == 1) {

$sql = "select id_inm from pagos where id = $id_pago";
$result_inm = $conn->query($sql);
$row_inm = $result_inm->fetch_assoc();

//print_r($row_inm);

$id_inm = $row_inm['id_inm'];

//echo PHP_EOL.$id_inm.PHP_EOL;

$sql = "select * from inmuebles where id = $id_inm";
$fecha = date('m-d-Y');
$ano_act = date('Y');
$mes_act = date('m');

$result11 = $conn->query($sql);
$row11 = $result11->fetch_assoc();


$mes_hasta = $row11['mes_hasta'];
$ano_hasta = $row11['ano_hasta'];

$ano_dif = $ano_act-$ano_hasta;
$mes_dif = $mes_act-$mes_hasta;

if ($ano_dif == 0) {
// echo $mes_dif;
    if ($mes_dif > 1) {
        //no solvente
        $estatus_inm = "no_solvente";
    } else {
        //solvente
        $estatus_inm = "solvente";
    }
} else {
 $estatus_inm = "no_solvente";
}

if(chequearReportePagoWeb($id_pago) == 0)
    $estatus = 'terminado';    
else
    $estatus = 'reportado';

}

$sql="update pagos set estatus = '$estatus' where id = $id_pago";
$conn->query($sql);

$id = $row['LAST_INSERT_ID()'];
$sql = "select * from x_pagos where id = $id";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

if ($saldo == 0 && $ag_ret == 1) $row['estatus'] = $estatus_inm;

$resp["error"] = false;
$resp["data"] = $row;


} else {
  $resp["error"] = true;
  $resp["msg_error"] =  $conn->error;
}


// print_r($resp);
echo json_encode($resp);
?>

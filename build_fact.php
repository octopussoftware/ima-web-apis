<?php
include('./connect.php');
include('./funciones_gen.php');
$id_pagos = $_POST['id_pagos'];
//verificar cuando se llama sin enviar id_pagos
$sql="select p.id,c.razon,c.ci_rif,c.dir,c.telf,p.fecha_fact,nro_factura,i.id id_inm,p.monto from pagos p 
inner join inmuebles i on (i.id = p.id_inm)
inner join contribuyente c on (c.id = i.id_contribuyente)
where p.id = $id_pagos";
$result = $conn->query($sql);
$header = $result->fetch_assoc();  

$id_inm = $header['id_inm'];

$sql="select i.id,tar.descripcion,parr.parr,z.zona,i.av_calle, e.edif, i.no_inmueble, i.piso, i.mts from inmuebles i 
inner join parroquias parr on (parr.id = i.id_parroquia)
inner join zonas z on (z.id = i.id_zona)
inner join edif e on (e.id = i.id_edif)
left join tarifas_com tar on (tar.id = i.id_actividad)
where i.id = $id_inm";//direccion de inmueble
$result = $conn->query($sql);
$inmdir = $result->fetch_assoc();

if ($inmdir['descripcion'] == '') {
	$tipo = "Residencial, Mts.: ".$inmdir['mts'];
    $exento = "(E)";
} else {
	$tipo = "Comercial / ".$inmdir['descripcion'].", Mts.: ".$inmdir['mts'];
    $exento = "";
}


$av_calle=$inmdir['av_calle'];
$no_inmueble=$inmdir['no_inmueble'];
$piso=$inmdir['piso'];

$dir_ubi = "Parroquia ".$inmdir['parr'].", Sector ".$inmdir['zona'].", ".$inmdir['edif'];

if (!is_null($av_calle)) $dir_ubi=$dir_ubi.", Av / Calle $av_calle";
if (!is_null($no_inmueble)) $dir_ubi=$dir_ubi.", $no_inmueble";
if (!is_null($piso)) $dir_ubi=$dir_ubi.", Piso $piso";


$sql="select * from d_pagos where id_pagos = $id_pagos and descripcion = 'reco' order by id_d_pagos desc";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$last_periodo = $row['periodo'];
$last_vec = explode("-",$last_periodo);
$to_mes= str_pad($last_vec[0], 2, '0', STR_PAD_LEFT);
$to=$to_mes."-".$last_vec[1];
$sql="select * from d_pagos where id_pagos = $id_pagos and descripcion = 'reco' order by id_d_pagos asc";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$first_periodo = $row['periodo'];
$first_vec = explode("-",$first_periodo);
$from_mes= str_pad($first_vec[0], 2, '0', STR_PAD_LEFT);
$from=$from_mes."-".$first_vec[1];


$sql="select sum(monto) as monto from d_pagos where descripcion = 'reco' and id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$monto_reco = $row['monto'];

$sql="select sum(monto) as monto from d_pagos where descripcion = 'int_reco' and id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$monto_int_reco = $row['monto'];

$sql="select sum(monto) as monto from d_pagos where descripcion = 'dispo' and id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$monto_dispo = $row['monto'];

$sql="select sum(monto) as monto from d_pagos where descripcion = 'int_dispo' and id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$monto_int_dispo = $row['monto'];

$sql="select sum(monto) as monto from d_pagos where descripcion = 'iva' and id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$monto_iva = $row['monto'];

// echo PHP_EOL.$monto_reco.PHP_EOL;
// echo PHP_EOL.$monto_int_reco.PHP_EOL;
// echo PHP_EOL.$monto_dispo.PHP_EOL;
// echo PHP_EOL.$monto_int_dispo.PHP_EOL;
// echo PHP_EOL.$monto_iva.PHP_EOL;

$sql="select sum(monto) as monto from d_pagos where descripcion = 'multa' and id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$monto_multa = number_format($row['monto'],2,'.','');
// echo PHP_EOL.$monto_multa.PHP_EOL;

$fechafact = date("d-m-Y", strtotime($header['fecha_fact']));

if (is_null($monto_iva)) $monto_iva=0;

//$total = $header['monto'];

$subtotal = number_format($monto_reco + $monto_dispo + $monto_int_reco + $monto_int_dispo,2,'.','');

$total = number_format($subtotal + $monto_iva,2,'.','');

// echo " rango $from  --->  $to ----  reco $monto_reco  ------  dispo   $monto_dispo ---- iva   $monto_iva ".PHP_EOL;
// $last_vec = explode("-",$last_periodo);
// $new_mes_hasta = str_pad($last_vec[0], 2, '0', STR_PAD_LEFT);
// $new_ano_hasta = $last_vec[1];
//echo json_encode($resp);
// exit;

$sql="select count(*) as cuantos from documentos where id_pagos = $id_pagos";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();

if ($row['cuantos'] == 0 ) {
$referencia = generardoc('fac');
$nro_control = generardoc('nro_ctrl_web');
$curr = date('Y-m-d H:i:s', time());
if ($monto_multa == 0) {
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('fac','$referencia',$id_pagos,'$curr','$curr','$curr','$nro_control','$total')";
$conn->query($sql);
  } else {
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('fac','$referencia',$id_pagos,'$curr','$curr','$curr','$nro_control','$total')";
$conn->query($sql);
$referencia_rec = generardoc('rec');
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('rec',$referencia_rec,$id_pagos,'$curr','$curr','$curr','N/A','$monto_multa')";
$conn->query($sql);
}

} else {

$sql="select * from documentos where id_pagos = $id_pagos";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();
$referencia = $row['referencia'];
}

// exit;

// $fechafact = date("d-m-Y", strtotime($header['fecha_fact']));

// if (is_null($monto_iva)) $monto_iva=0;

// $total = $header['monto'];

// $subtotal = $total - $monto_iva;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>FACTURA DE PAGO IMA</title>
<!-- <link href="assets/css/fact-styles/factura.css" rel="stylesheet" type="text/css" /> -->
</head>

<body onload="window.print()">

<div class="contenerdorFactura">
	<div class="cuadroTop">
    	<div class="cuadroTopDer">
        	<div class="lineTop"><b>Razón Social/Nombre: </b><? echo $header['razon']?></div>
            <div class="lineTop">
            	<b>RIF / CI: </b><? echo $header['ci_rif']?>                            </div>
            <div class="lineTop">
            	<b>Dirección Fiscal: </b><? echo $header['dir']?>
                <span id="dirfac"></span>
            </div>
            <div class="lineTop">
            	<b>Tipo de Inmueble: </b><? echo $tipo?></br>
            	<b>Datos del Inmueble: </b><? echo $dir_ubi?>
            	<span id="dirfac"></span>
            </div>
        </div>
        <div class="cuadroTopIzq">
        	<div class="lineTop"><b>N° Factura: <? echo $referencia?><br> 
        	Serie </b></div>
            <div class="lineTop"><b>Fecha de Emisión: </b><? echo $header['fecha_fact']?></div>
                        <div class="lineTop"><b>Usuario: </b>Cajero Web </b></div>
                    </div>
    </div>
    <div class="cuadroCenter">
    	<div class="lineCemter">
        	<div class="item">
        	  <div align="center"><B>CONCEPTO</B></div></div>
            <div class="monto">
              <div align="center"><B>TOTAL</B></div></div>
        </div>    
        <div class="lineSeparador"></div>
                <div class="lineCemter">
        	<div class="item"><div align="left">Servicio de Recolección Aseo Urbano. Período <? echo $from?> al <? echo $to?> <? echo $exento?></div></div>
            <div class="monto"><div align="center"><? echo number_format($monto_reco,2,',','.')?></div></div>
        </div> 
                <div class="lineCemter">
        	<div class="item"><div align="left">Servicio de Disposición final. Período <? echo $from?> al <? echo $to?> <? echo $exento?></div></div>
            <div class="monto"><div align="center"><? echo number_format($monto_dispo,2,',','.')?></div></div>
        </div>
       <? if ($inmdir['descripcion'] != '') {?>

        <div class="lineCemter">
            <div class="item"><div align="left">Intereses Recolección Aseo Urbano. Período <? echo $from?> al <? echo $to?> <? echo $exento?></div></div>
            <div class="monto"><div align="center"><? echo number_format($monto_int_reco,2,',','.')?></div></div>
        </div>
        <div class="lineCemter">
            <div class="item"><div align="left">Intereses Disposición Aseo Urbano. Período <? echo $from?> al <? echo $to?> <? echo $exento?></div></div>
            <div class="monto"><div align="center"><? echo number_format($monto_int_dispo,2,',','.')?></div></div>
        </div>  <? }  ?>
                
    </div>
    <div class="lineSeparador"></div>
    <div class="cuadroFooter">
    <div class="cuadroFormas">
    			<div>Este servicio está exento del 2% del ISLR </div>
              <div id="forma"><b>Formas de pago:</b></div>
                </div>
    	<div class="cuadroTotales">
        	<div class="lineTotales">
            	<div class="totalesIzq"><b>SUB-TOTAL  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($subtotal,2,',','.')?></div></div>            	
            </div>
            <div class="lineTotales">
            	<div class="totalesIzq"><b>IVA 12%  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($monto_iva,2,',','.')?></div></div>             	
            </div>
            <div class="lineTotales">
            	<div class="totalesIzq"><b>TOTAL  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($total,2,',','.')?></div></div>            	
            </div>
        </div>    
    </div>
</div>
</body>  

<style>
    @charset "utf-8";
/* CSS Document */
body{
    font-family:"Segoe UI";
}
.contenerdorFactura{
    width:900px;
    overflow:hidden;
    margin-top:100px;
}
.contenerdorFactura2{
    width:900px;
    overflow:hidden;
    margin-top:0px;
}
.contenerdorFactura3{
    width:900px;
    overflow:hidden;
    margin-top:180px;
}
.contenerdorFactura2{
    width:900px;
    overflow:hidden;
}
.cuadroTop{
    width:100%;
    overflow:hidden;
}
.cuadroTopDer{
    height:130px;
    width:65%;
    float:left;
}
.cuadroTopIzq{
    height:80px;
    width:33%;
    float:left;
    padding-left:10px;
    padding-top:40px;
    
}
.lineTop{
    width:100%;
    height:20px auto;
}
.cuadroCenter{
    width:100%;
    margin-top:10px;
    height:200px;
}
.cuadroCenterEstado{
    width:100%;
    margin-top:10px;
    height: auto ;
}
.lineCemter{    
    width:100%;
    height:20px;
}
.item{
    width:70%;
    float:left;
}
.monto{
    width:27%;
    float:left;
    margin-left:1%
}
.lineSeparador{
    width:100%;
    border-top:2px #000000 solid;
    margin-top:5px;
}
.lineSeparador2{
    width:100%;
    border-top:2px #000000 solid;
    margin-top:5px;
    overflow:hidden;
}
.cuadroFooter{
    width:100%;
    overflow:hidden;
}
.cuadroTotales{
    width:30%;
    float:right;
}
.cuadroFormas{
    width:68%;
    float:left;
}
.lineTotales{
    width:100%;
    border:#FF0;
    overflow:hidden;
}
.totalesIzq{
    width:47%;
    padding-right:5px;
    float:left;
    text-align: right;
}
.totalesDer{
    width:47%;
    padding-left:5px;
    float:left;
    text-align: left;
}
.tituloEstado{
    border-top:1px #000000 solid;
    border-bottom:1px #000000 solid;
    text-align:center;
    font-size:18px;
}
#forma{
    width:auto;
    float:left;
    margin-right:3px;
    font-size:12px;
}
#dirfac{
    font-size:12px;
}

.centro{
    font-size: 12px;
    text-align: left;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}

.menbrete{
    background-color:#3BFF17;
    color:#FFFFFF;
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}
.menbrete2{
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}

.imgmenbrete{
    float:right;
    z-index:2;
}

.floor{
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:9px;
}


</style>                    

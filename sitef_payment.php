<?php

include('./sitef/sitef.php');
include('./sitef/credentials.php');

$paymentData = $_POST;

/*
Array
(
    [cvv] => 123
    [number] => 454545454545234
    [name] => Oswald
    [mon] => 02
    [year] => 2018 
    [cedRif] => 121112121
    [cedRifCode] => J 
    [tlf] => 04144545454
    [amount] => 5676.66
    [cardType] => 1
    [id_pago] => 'id_pago'
)
*/

$paymentData['year'] = substr($paymentData['year'], 0, 2);

switch(substr($paymentData['number'], 0, 1)){
	case '4':
		$paymentData['cardType'] = '1';
	break;
	case '5':
		$paymentData['cardType'] = '2';
	break;
	case '3':
		$paymentData['cardType'] = '3';
	break;
}

// print_r($paymentData);

$begin = sitefBeginTransaction();

if($begin['error']){
	echo json_encode($begin['data']);
	die();
}

$payment = sitefDoPayment($begin['data']['nit']);

echo json_encode($payment);

function sitefBeginTransaction(){ 

	global $paymentData;

  $transaction = new SitefPayment();
  $result      = $transaction->beginTransaction($paymentData['amount'], $paymentData['id_pago']);  

  if(isset($result['error']))
    return array(
      'error' => true,
      'data'  => $result);

  return array(
      'error' => false,
      'data'  => $result);
}

//Do Payment sitef process
function sitefDoPayment($nit){

	global $paymentData;
  global $credentials;

  //Card
  $number = $paymentData['number'];     //<= 19 digitos
  $type   = $paymentData['cardType'];   //<= 3 digitos //1 visa 2 mastercard 3 american express  33 diners 
  $date   = $paymentData['mon'].$paymentData['year'];  //=4 digitos 
  $cvv    = $paymentData['cvv'];    //<=5 digitos

  //Card Holer
  $cedRif     = $paymentData['cedRif'];  //<= 20 caracteres
  $cedRifCode = $paymentData['cedRifCode'];  //<= 20 caracteres
  $name       = $paymentData['name'];   //<= 50 caracteres

  ///ORDER
  $amount   = $paymentData['amount'] * 100;  //<= 12   digitos
  $orderId  = $paymentData['id_pago'];  //<= 20 caracteres
  $storeUSN = $credentials['storeUSN'];   //<=12  digitos

  $transaction = new SitefPayment();
  $result      = $transaction->doPayment($nit, $number, $type, $name, $cedRifCode, $date, $cvv);    

  return $result;

}

?>

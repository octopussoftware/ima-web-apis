
<?php

include('../database.php');

try{

	$parrs = $db->table('parroquias')
		->orderBy('parr', 'asc')
		->get();

	$resp['data'] = $parrs;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

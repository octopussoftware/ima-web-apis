<?php
include('../database.php');

$id_pagos = $_POST['id_pagos'];
if (isset($_POST['ag_ret'])) {$ag_ret = $_POST['ag_ret'];}

try{
 
	$x_pagos = $db->table('x_pagos')
		->where(array('id_pagos' => $id_pagos))
		->where('id_tipo_pago', '!=', 75)
		->orderBy('id', 'asc')
		->get();

	$d_pagos = $db->table('d_pagos')
		->where(array('id_pagos' => $id_pagos))
		->orderBy('id_d_pagos', 'asc')
		->get();

$deta_pago=[];
$total_reco=0;
$total_int_reco=0;
$total_dispo=0;
$total_int_dispo=0;
$periodos_vec=[];

$abonos = 0;

	foreach ($x_pagos as $array) {

		// print_r($array);
		$abonos = $abonos + $array->monto;
	}

	foreach($d_pagos as $vector) {

		

		$vector = json_decode(json_encode($vector), True);
		// echo $vector->id_pagos;

		// print_r($vector);

		// exit;

		// print_r($vector);

		$periodo = $vector['periodo'];
		$descripcion = $vector['descripcion'];

		if (!isset($deta_pago[$periodo]['total'])) {
			$deta_pago[$periodo]['total'] = 0;
		}
		// exit;

switch ($descripcion)
{
	case 'reco':   //efectivo
	$deta_pago[$periodo]['reco'] = $vector['monto'];
	$deta_pago[$periodo]['total'] = $deta_pago[$periodo]['total'] + $vector['monto'];
	$total_reco = $total_reco + $vector['monto'];
    break;

    case 'dispo':   //efectivo
	$deta_pago[$periodo]['dispo'] = $vector['monto'];
	$deta_pago[$periodo]['total'] = $deta_pago[$periodo]['total'] + $vector['monto'];
	$total_dispo = $total_dispo + $vector['monto'];
    break;

    case 'int_reco':   //efectivo
	$deta_pago[$periodo]['int_reco'] = $vector['monto'];
	$deta_pago[$periodo]['total'] = $deta_pago[$periodo]['total'] + $vector['monto'];
	$total_int_reco = $total_int_reco + $vector['monto'];
    break;

    case 'int_dispo':   //efectivo
	$deta_pago[$periodo]['int_dispo'] = $vector['monto'];
	$deta_pago[$periodo]['total'] = $deta_pago[$periodo]['total'] + $vector['monto'];
	$total_int_dispo = $total_int_dispo + $vector['monto'];
    break;
}
	}

// print_r($deta_pago);
// exit;

$grantotal = number_format($total_reco, 2, '.', '') + number_format($total_dispo, 2, '.', '');

// $iva = 0.12*number_format($grantotal, 2, '.', '');





$total_pagos = number_format($grantotal, 2, '.', '');

	$totals = array(
	 'reco' => number_format($total_reco, 2, '.', ''),
	 'dispo' => number_format($total_dispo, 2, '.', ''),
	 'subtotal' => number_format($grantotal, 2, '.', ''),
	 'total' => number_format($total_pagos, 2, '.', ''),
	 'abonos' => number_format($abonos, 2, '.', ''),
	 'saldo' => number_format($total_pagos-$abonos, 2, '.', ''),
	 'id_pagos' => $id_pagos
	);



foreach ($deta_pago as $key=>$array) {

	if(in_array($key,array('multa','iva','total','total_pagos','ret_iva'))) continue;
	// echo $array['reco'].PHP_EOL;
	array_push($periodos_vec, array(
		'periodo' => $key,
		'reco' => number_format($array['reco'], 2, '.', ''),
		'dispo' => number_format($array['dispo'], 2, '.', ''),
		'total' => number_format($array['total'], 2, '.', '')
	));

}
// print_r($periodos_vec);
// print_r($deta_pago);
// // echo json_encode($deta_pago);
// exit;

	$resp['data']['periodos'] = $periodos_vec;
	$resp['data']['pagos'] = $x_pagos;
	$resp['data']['totals'] = $totals;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

// print_r($resp);
echo json_encode($resp);

?>

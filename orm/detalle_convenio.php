<?php
include('../database.php');
include('../connect.php');
$id_convenio = $_POST['id_convenio'];
$ag_ret = $_POST['ag_ret']; 


try{

	$convenios = $db->table('x_convenio')
		->where(array('id_convenio' => $id_convenio))
		->orderBy('id','asc')
		->get();

    $out_convenios=[];
	$curr = date('Y-m-d');

foreach ($convenios as $array) {
	$array = json_decode(json_encode($array), True);
	$fecha_vence=$array['fecha_vence'];

	$id_x_convenio = $array['id'];

	$sql="select * from pagos where tipo_pago = 'conv' and id_doc = $id_x_convenio";
	$rs = $conn->query($sql);
	$row = $rs->fetch_assoc();

// echo $sql.PHP_EOL;
// print_r($array);
// print_r($row);
	 


	$id_pagos = $row['id'];

	if (isset($row['monto_multa'])) {
	$multa = $row['monto_multa'];
	} else {
	$multa = 0;	

	if ($curr > $fecha_vence ) {

	  	$multa = 30*300;
	 }



	}
    

	// $multa = 0;
	//  $estatus_cuota = $array['estatus'];

	//  if ($curr > $fecha_vence && $estatus_cuota != 'pagada') {

	//  	$multa = 999;
	//  }




	$estatus_pago = $row['estatus'];

	$monto_cuota = $array['monto_cuota'];
	$monto_iva = number_format(0.12*$array['monto_cuota'],2,'.','');
	$monto_cuota_iva = number_format($monto_cuota + $monto_iva + $multa,2,'.','');

	$array['monto_iva'] = $monto_iva;
	$array['monto_cuota_iva'] = $monto_cuota_iva;
	$array['nro_control'] = $row['nro_control'];
	$array['multa'] = $multa;



if ($id_pagos == '') {

$pagado = 0;
$monto_total=$monto_cuota_iva;

}	else {
//////////////////////////////////////////////////////////////////////////////

$sql="select count(*) as cuantos from x_pagos where id_pagos = $id_pagos";

// echo $sql;
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$cuantos = $row['cuantos'];

if ($cuantos == 0) {

$pagado = 0;
$monto_total=$monto_cuota_iva;

} else {

$sql="select sum(x.monto) as pagado,p.monto from x_pagos x
inner join pagos p on (p.id = x.id_pagos)
where x.id_pagos = $id_pagos group by p.monto";
// echo $sql;
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$pagado = $row['pagado'];
$monto_total = $row['monto'];

}

} //id pago es null

$array['abonos'] = $pagado;
//$array['monto_total'] = $monto_total;
$array['saldo'] = number_format($monto_total - $pagado + $multa,2,'.','');


// print_r($row);
///////////////////////////////////////////////////////////////////////////////
	
if ($id_pagos != '')  {
	// if ($array['id_pagos'] != '')  {
			$array['estatus'] = $estatus_pago;
		
     
	
	} else {

		if ($ag_ret == 1) {
			$array['estatus'] = "imp_fact";
		} else {
			$array['estatus'] = "cargar_pagos";
		}

	}
	array_push($out_convenios,$array);
	}
	$resp['data'] = $out_convenios;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

// print_r($resp);
echo json_encode($resp);
                         
?>

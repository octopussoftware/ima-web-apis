<?php

include('../database.php');

try{

	$user = $db->table('usuarios')
		->where('id', $_POST['id'])
		->first();

	$resp['data'] = $user;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

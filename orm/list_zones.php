<?php

include('../database.php');

$id_parr = $_POST['id_parr'];

try{

	$zones = $db->table('zonas')
		->where(array('id_parr' => $id_parr))
		->orderBy('zona', 'asc')
		->get();

	$resp['data'] = $zones;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

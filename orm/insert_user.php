<?php

include('../database.php');

$postedData = $_POST;
unset($postedData['code']);
unset($postedData['codType']);
unset($postedData['cedRif']);
unset($postedData['telfCode']);
unset($postedData['telfBody']);

try{

	$contrib = $db->table('contribuyente')
		->where('ci_rif', $postedData['ci_rif'])->first();

	if(isset($contrib->id)){
		$resp['exist'] = true;
	}else{
		$resp['exist'] = false;
		$id = $db->table('contribuyente')->insertGetId($postedData);
		$resp['data'] = $db->table('contribuyente')->find($id);
	}

	$resp['error'] = false;
}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

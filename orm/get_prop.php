<?php

include('../database.php');

$id_inm = $_POST['id_inm'];

try{

	$prop = $db->table('v_list_props')
		->where(array('id' => $id_inm))
		->first();

	$payment = $db->table('pagos')
		->where('estatus', '!=', 'terminado')
		->where('estatus', '!=', 'nuevo')
		->where('id_inm', $id_inm)
		->select('id')->first();

	$prop->id_pagos = $payment->id;
	
	$resp = array(
		'error' => false, 
		'data' => $prop);

}catch(Illuminate\Database\QueryException $e){
	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();
}

echo json_encode($resp);

?>

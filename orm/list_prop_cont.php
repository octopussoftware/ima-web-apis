<?php

include('../database.php');

$ci_rif = $_POST['ci_rif'];


try{
	$cont = $db->table('contribuyente')
		->where(array('ci_rif' => $ci_rif))
		->first();

	if (!isset($cont->id)) {
		echo json_encode(array('data' => null, 'error' => false));
		die();
	}

	$props = $db->table('v_list_props')
		->where(array('id_contribuyente' => $cont->id))
		->get();


$out_props=[];

	foreach ($props as $array) {
		// print_r($array);

///////////////////////////
$array = json_decode(json_encode($array), True);


include('../connect.php');

$id_inm = $array['id'];
//$id_inm = 23 ;
$sql = "select * from inmuebles where id = '$id_inm'";
$fecha = date('m-d-Y');
$ano_act = date('Y');
$mes_act = date('m');

$result = $conn->query($sql);
$row = $result->fetch_assoc();


$mes_hasta = $row['mes_hasta'];
$ano_hasta = $row['ano_hasta'];

$ano_dif = $ano_act-$ano_hasta;
$mes_dif = $mes_act-$mes_hasta;

if ($ano_dif == 0) {
// echo $mes_dif;
	if ($mes_dif > 1) {
		//no solvente
		$estatus = "no_solvente";
	} else {
    	//solvente
    	$estatus = "solvente";
	}
} else {
 $estatus = "no_solvente";
}

$id_pagos = null;

$sql2="select * from pagos where id_inm = '$id_inm' and estatus != 'terminado' and estatus != 'nuevo'";
// and estatus in ('en_proceso','en_proceso_ret')";
$result2 = $conn->query($sql2);
$row2 = $result2->fetch_assoc();

//print_r($row2);
if (count($row2) != 0) {

$estatus_pago = $row2['estatus'];
$id_pagos = $row2['id'];

switch ($estatus_pago)
{
    case 'nuevo':
       //echo "00000000000000";
        break;
    
	default:
		$estatus = $estatus_pago;
		break;
}
}

// $estatus = 'malo';

$array['estatus'] = $estatus;
$array['id_pagos'] = $id_pagos;




array_push($out_props,$array);
// echo $array['estatus'];

///////////////////////////////////////////////////////////


	}

	$resp = array(
		'error' => false, 
		'data' => array (
			'contribuyente' => $cont,
			'inmuebles' => $out_props
		));

}catch(Illuminate\Database\QueryException $e){
	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();
}

echo json_encode($resp);

?>

<?php

include('../database.php');

$id_cont = $_POST['id_cont'];

try{

	//Array with id_inm
	$props = $db->table('v_list_props')
		->where('id_contribuyente', $id_cont)
		->get();

	$inm_ids = [];
	foreach ($props as $key => $value)
		array_push($inm_ids, $value->id);

	$payments = $db->table('pagos')
		->whereIn('id_inm', $inm_ids)
		->orderBy('id', 'desc')
		->get();

	$resp['data'] = array(
		'inmuebles' => $props,
		'pagos' => $payments);
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

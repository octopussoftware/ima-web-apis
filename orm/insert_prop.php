<?php

include('../database.php');

$postedData = $_POST;

//Discard unnecessary fields
unset($postedData['uso']);

if($postedData['id_uso'] === '1')
	unset($postedData['id_actividad']);

$postedData['fecha_crea'] = date("Y/m/d");
$postedData['fecha_mod'] = date("Y/m/d");
$postedData['estatus'] = 'nuevo';

try{

	$newPropId = $db->table('inmuebles')->insertGetId($postedData);

	$newProp = $db->table('v_list_props')->where('id', $newPropId)->first();

	$resp['data'] = $newProp;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

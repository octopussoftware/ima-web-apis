<?php

include('../database.php');

$id_cont = $_POST['id_cont'];

try{

	$docs = $db->table('v_list_fact_docs')
		->where(array('id_cont' => $id_cont, 'tipo' => 'conv'))
		->get();

	$resp['data'] = $docs;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

// print_r($resp);
echo json_encode($resp);
                         
?>

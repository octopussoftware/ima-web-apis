
<?php

include('../database.php');

try{

	$levels = $db->table('niveles')
		->orderBy('nivel', 'asc')
		->get();

	$resp['data'] = $levels;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

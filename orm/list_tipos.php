<?php

include('../database.php');

try{

	$types = $db->table('tipos')
		->where('clase', $_POST['clase'])
		->orderBy('tipo', 'asc')
		->get();

	$resp['data'] = $types;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

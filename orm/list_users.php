<?php

include('../database.php');

try{

	$users = $db->table('usuarios')
		->get();

	foreach ($users as $key => $value)
		unset($users[$key]->password);

	$resp['data'] = $users;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

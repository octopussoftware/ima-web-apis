<?php

include('../database.php');

$updateData = $_POST;

try{

	$db->table('usuarios')
		->where('id', $updateData['id'])
		->update($updateData);

	$user = $db->table('usuarios')->find($updateData['id']);

	$resp['data'] = $user;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

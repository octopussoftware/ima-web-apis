<?php

include('../database.php');

$id = $_POST['id'];
$mail = $_POST['correo'];

$initialPass = substr(md5(date(DATE_RFC2822)), 0, 6);
$pass = md5($initialPass);

try{

	$db->table('contribuyente')->where('id', $id)
		->update(array('correo' => $mail, 'pass' => $pass));

	$resp['error'] = false;
	$resp['data'] = array('pass' => $initialPass, 'email' => $mail);
}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

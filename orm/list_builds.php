<?php

include('../database.php');

$id_zona = $_POST['id_zone'];

try{

	$edifs = $db->table('edif')
		->where(array('id_zona' => $id_zona))
		->orderBy('edif', 'asc')
		->get();

	$resp['data'] = $edifs;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

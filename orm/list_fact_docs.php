<?php

include('../database.php');

$id_cont = $_POST['id_cont'];

try{

	$docs = $db->table('v_list_fact_docs')
				->where(array('id_cont' => $id_cont))
				->whereIn('tipo',array('rec','fac'))
				->get();
	

	$out_docs=[];

	foreach ($docs as $array) {
	$array = json_decode(json_encode($array), True);
	
	$tipo_pago=$array['tipo_pago'];
	$tipo_doc =$array['tipo'];

	if ($tipo_pago == 'aseo' && $tipo_doc == 'fac') $concepto = 'Factura Aseo';
	if ($tipo_pago == 'aseo' && $tipo_doc == 'rec') $concepto = 'Multa Aseo';
	if ($tipo_pago == 'conv' && $tipo_doc == 'fac') $concepto = 'Factura Convenio';
	if ($tipo_pago == 'conv' && $tipo_doc == 'rec') $concepto = 'Multa Convenio';

	$array['concepto'] = "$concepto";

	array_push($out_docs, $array);

	}



	$resp['data'] = $out_docs;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}
// print_r($resp);
echo json_encode($resp);
                         
?>



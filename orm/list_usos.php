<?php

include('../database.php');

try{

	$uses = $db->table('usos')
		->orderBy('uso', 'asc')
		->get();

	$resp['data'] = $uses;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

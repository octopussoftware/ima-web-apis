<?php

include('../database.php');

$ci_rif = $_POST['ci_rif'];
unset($_POST['code']);
unset($_POST['codType']);
unset($_POST['cedRif']);

try{

	$contrib = $db->table('contribuyente')->where('ci_rif', $ci_rif)->first();

	if(isset($contrib->id)){
		$resp['exist'] = true;

		if($contrib->pass)
			$resp['logged'] = true;
		else
			$resp['logged'] = false;

		$resp['id'] = $contrib->id;
		$resp['mail'] = $contrib->correo;
	}else{
		$resp['exist'] = false;
	}

	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

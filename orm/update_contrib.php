<?php

include('../database.php');

$postedData = $_POST;

try{

	$db->table('contribuyente')->where('id', $postedData['id'])
		->update($postedData);

	//Return updated user
	$contrib = $db->table('contribuyente')->where('id', $postedData['id'])
		->first();

	unset($contrib->pass);

	$resp['error'] = false;
	$resp['data'] = $contrib;
}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

                         
?>

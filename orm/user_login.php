<?php

include('../database.php');

$postedData = $_POST;
$pass = md5($postedData['pass']);

try{

	$user = $db->table('usuarios')
		->where(array('login' => $postedData['login'], 'password' => $pass))
		->first();

	if(isset($user->id)){
		unset($user ->password);
		$resp['data'] = $user;
	}else{
		$resp['data'] = null;
	}

	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}


echo json_encode($resp);
                         
?>

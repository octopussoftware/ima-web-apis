<?php

include('../database.php');

$postedData = $_POST;


unset($postedData['uso']);
unset($postedData['tipo']);
unset($postedData['actividad']);
unset($postedData['catastro']);
unset($postedData['patente']);
unset($postedData['parr']);
unset($postedData['zona']);
unset($postedData['edif']);
unset($postedData['fecha_crea']);
unset($postedData['estatus']);

if($postedData['id_uso'] === '1')
	unset($postedData['id_actividad']);

$postedData['fecha_mod'] = date("Y/m/d");

try{

	$db->table('inmuebles')
		->where(array('id' => $postedData['id'], 'id_contribuyente' => $postedData['id_contribuyente']))
		->update($postedData);

	$prop = $db->table('v_list_props')
		->where(array('id' => $postedData['id']))
		->first();

	$resp['error'] = false;
	$resp['data'] = $prop;
}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

                         
?>

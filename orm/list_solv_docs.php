<?php

include('../database.php');

$id_cont = $_POST['id_cont'];

try{

	$docs = $db->table('v_list_solv_docs')
		->where(array('id_cont' => $id_cont, 'tipo' => 'sol'))
		->get();

	$out_docs=[];
	$curr = date('Y-m');

		foreach ($docs as $array) {
	$array = json_decode(json_encode($array), True);
	$fecha_doc=$array['fecha_doc'];

	if ($curr == date('Y-m',strtotime($fecha_doc))) {
	$array['estatus'] = "valida";
	} else {
	$array['estatus'] = "no_valida";
	}

	array_push($out_docs,$array);

	}

	$resp['data'] = $out_docs;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

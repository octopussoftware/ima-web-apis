<?php

include('../database.php');

$postedData = $_POST;
$pass = md5($postedData['pass']);

try{

	$contrib = $db->table('contribuyente')
		->where(array('ci_rif' => $postedData['ci_rif'], 'pass' => $pass))
		->first();

	if(isset($contrib->id)){
		unset($contrib ->pass);
		$resp['data'] = $contrib;
	}else{
		$resp['data'] = null;
	}

	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}


echo json_encode($resp);
                         
?>

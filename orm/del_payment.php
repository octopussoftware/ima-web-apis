<?php

include('../database.php');

$postedData = $_POST;
$fecha_mod = date('Y/m/d');

try{

	$db->table('x_pagos')->where('id', $postedData['id_x_pago'])
		->update(array('monto' => 0, 'estatus' = 'eliminado', 'fecha_mod' => '$fecha_mod'));

	//Return updated user
	$payment = $db->table('x_pagos')->where('id', $postedData['id_x_pago']);
		->first();

	$resp['error'] = false;
	$resp['data'] = $payment;
}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);


                         
?>

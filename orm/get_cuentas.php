<?php

include('../database.php');

try{

	$cuentas = $db->table('v_cuentas')
		->orderBy('cuenta', 'asc')
		->get();

	$resp['data'] = $cuentas;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

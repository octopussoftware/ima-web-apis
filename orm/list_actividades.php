<?php

include('../database.php');

try{
	// $activities = $db->table('actividades')
	// 	->orderBy('actividad', 'asc')
	// 	->get();

	$activities = $db->table('tarifas_com')
		->orderBy('descripcion', 'asc')
		->get(['id', 'descripcion as actividad']);

	$resp['data'] = $activities;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

<?php

include('../database.php');

try{	

	$cont = $db->table('contribuyente')
		->where(array('id' => $_POST['id']))
		->first();

	$resp['data'] = $cont;
	$resp['error'] = false;

}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);

?>

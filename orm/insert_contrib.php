<?php

include('../database.php');

$postedData = $_POST;
unset($postedData['code']);
unset($postedData['cedRif']);

try{

	$contrib = $db->table('contribuyente')->where('ci_rif', $postedData['ci_rif'])->first();

	if(isset($contrib->id)){
		$resp['exist'] = true;
	}else{
		$resp['exist'] = false;
		$db->table('contribuyente')->insert($postedData);
	}

	$resp['error'] = false;
}catch(Illuminate\Database\QueryException $e){

	$resp['error'] = true;
	$resp['msg'] = $e->getMessage();

}

echo json_encode($resp);
                         
?>

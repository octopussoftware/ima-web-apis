<?
include('./connect.php');
include('./funciones_gen.php');

$id_inm = $_POST['id_inm'];

$curr = date('Y-m-d', time());

$sql="select count(*) as cuantos from documentos where tipo = 'dec' and id_pagos = $id_inm";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();

if ($row['cuantos'] == 0) {
 $referencia = generardoc('dec');   

$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('dec','$referencia',$id_inm,'$curr','$curr','$curr','N/A',0)";
$conn->query($sql);
} else {
$sql="select * from documentos where tipo = 'dec' and id_pagos = $id_inm";   //OJO  en el caso de las dec el id_pagos es id_inmueble parecido a las tipo conv
$rs = $conn->query($sql);
$row = $rs->fetch_assoc(); 

$referencia = $row['referencia'];

}


$sql="select i.catastro,i.nic,c.id as id_cont,p.id,c.razon,c.ci_rif,c.dir,c.telf,p.fecha_fact,nro_factura,i.id id_inm,p.monto from pagos p 
inner join inmuebles i on (i.id = p.id_inm)
inner join contribuyente c on (c.id = i.id_contribuyente)
where i.id = $id_inm";
$result = $conn->query($sql);
$header = $result->fetch_assoc();

$sql="select i.id,tar.descripcion,parr.parr,z.zona,i.av_calle, e.edif, i.no_inmueble, i.piso, i.mts from inmuebles i 
inner join parroquias parr on (parr.id = i.id_parroquia)
inner join zonas z on (z.id = i.id_zona)
inner join edif e on (e.id = i.id_edif)
left join tarifas_com tar on (tar.id = i.id_actividad)
where i.id = $id_inm";//direccion de inmueble
$result = $conn->query($sql);
$inmdir = $result->fetch_assoc();

if ($inmdir['descripcion'] == '') {
  $tipo = "Residencial, Mts.: ".$inmdir['mts'];
    $exento = "(E)";
} else {
  $tipo = "Comercial / ".$inmdir['descripcion'].", Mts.: ".$inmdir['mts'];
    $exento = "";
}

$av_calle=$inmdir['av_calle'];
$no_inmueble=$inmdir['no_inmueble'];
$piso=$inmdir['piso'];

$dir_ubi = "Parroquia ".$inmdir['parr'].", Sector ".$inmdir['zona'].", ".$inmdir['edif'];

if (!is_null($av_calle)) $dir_ubi=$dir_ubi.", Av / Calle $av_calle";
if (!is_null($no_inmueble)) $dir_ubi=$dir_ubi.", $no_inmueble";
if (!is_null($piso)) $dir_ubi=$dir_ubi.", Piso $piso";



//Root server
$server = 'http://ima.gob.ve';
// $server = 'http://localhost:8033';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DECLARACIÓN JURADA RAV</title>
<!-- <link href="../css/factura_nue.css" rel="stylesheet" type="text/css" /> -->
<style>
#firma{
	width:230px;
	height:70px;
	z-index:2;
	position: static;
	margin-bottom:-30px;
}
</style>
</head>

<body>

<table width="900" height="420" border="0">
  <tr>
    <td height="23">
      <table width="980" border="0" align="right">
        <tr>
          <td width="246">
          	<strong>
            <img src="<? echo $server ?>/assets/img/docs/agree/escudo.jpg" width="127" height="129" />
            </strong>
           </td>
          <td width="454" align="center">&nbsp;</td>
          <td width="176"><img src="<? echo $server ?>/assets/img/docs/agree/logo_ima.jpg" width="280" height="129" /></td>
        </tr>
    </table>
    </td>
  </tr>
  
    <td align="right">
    	<p align="right">Valencia, <?=date("d/m/Y")?><br />
    	Código: IMA-<strong><? echo $referencia?>
    	</strong>/2017 
        </p>
    </td>
    
  		<tr>
    <td>
    
    <table width="900" border="0">
      <tr>
      
        <td align="left">
          <p align="right">Fecha: <?=date("d/m/Y")?></p>
          <p align="center"><strong>DECLARACIÓN JURADA</strong></p>
          <p align="justify">NOMBRE O RAZÓN SOCIAL: <strong><? echo strtoupper($header['razon'])?></strong> C.I. o R.I.F: <strong><? echo $header['ci_rif']?></strong> DIRECCIÓN FISCAL: <? echo strtoupper($header['dir'])?> NIC: <? echo $header['nic']?> CÓDIGO IMA: <? echo $header['id']?> No Paga Actualmente DATOS DEL INMUEBLE: CEDULA CATASTRAL: <? echo $header['catastro']?> DIRECCIÓN: <? echo strtoupper($dir_ubi)?> YO: <? echo strtoupper($header['razon'])?> C.I. NRO: <? echo $header['ci_rif']?> EN MI CONDICION DE USUARIO DEL SERVICIO DE ASEO URBANO Y DOMICILIARIO, Y EN CUMPLIMIENTO CON LO DISPUESTO EN LOS ARTICULOS 35 Y 36 DE LA ORDENANZA SOBRE EL SERVICIO PUBLICO DE ASEO URBANO Y DOMICILIARIO VIGENTE, DECLARO QUE LOS DATOS SUMINISTRADOS, RECAUDADOS Y CONSIGNADOS EN ESTA PLANILLA SON VERDADEROS. POR LO QUE CUALQUIER IRREGULARIDAD, SERÁ OBJETO DE SANCIÓN DE ACUERDO A LO ESTABLECIDO EN EL ARTÍCULO 55 DEL LEY SOBRE SIMPLIFICACIÓN DE TRÁMITES ADMINISTRATIVOS FECHA MARTES 18 DE NOVIEMBRE DE 2014, DECRETO PRESIDENCIAL N° 1.423 “Artículo 55.- Sin perjuicio de su responsabilidad civil y penal, los particulares que hayan suministrado datos falsos en el curso de las tramitaciones administrativas a que se refiere el artículo 30 del presente decreto-Ley serán sancionados con multa cuyo monto se determinará entre 6,25 y 25 Unidades Tributarias, según la gravedad de la infracción.” OBLIGADOS DEL PAGO DEL SERVICIO DE ASEO URBANO Y DOMICILIARIO ARTÍCULO 14: Todos los usuarios del servicio público de aseo urbano y domiciliario están obligados al pago de las tasas que le corresponden según lo establecido en esta Ordenanza. ARTÍCULO 15: El propietario de un inmueble es solidariamente responsable de la obligación de pago por la prestación del servicio público de aseo urbano y domiciliario, aún cuando el inmueble se encuentre ocupado por otra persona, vacio, sin uso o desocupado.(ORDENANZA SOBRE EL SERVICIO PUBLICO DE ASEO URBANO Y DOMICILIARIO) Firma del Tramitante NOTA: TODOS LOS CAMPOS INDICADOS EN ESTA PLANILLA DEBEN SER LLENADOS, LA OMISIÓN DE ALGÚN DATO PUEDE SER MOTIVO DE RECHAZO A SU SOLICITUD.</p>

            
			<table width="800" border="0">
 				<tr>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
    			<td>&nbsp;</td>
				</tr>

			</table>
            
            <table width="980" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td width="374" valign="top"><strong>  Por el Instituto Municipal del Ambiente (IMA)</strong></td>
                <td width="185" valign="top"><p align="center">&nbsp;</p></td>
                <td width="461" valign="top"><p align="center"><strong>Por el usuario</strong></p></td>
              </tr>
              <tr>
                <td width="374" valign="top"></td>
                <td width="185" valign="top"></td>
                <td width="461" valign="top"></td>
              </tr>
              <tr>
                <td width="374" valign="top" align="center">
                  <div id="firma">
                    <img src="<? echo $server ?>/assets/img/docs/agree/joel.png" width="230" height="69" />
                  </div>
                  _____________________________</td>
               
                <td width="185" valign="top" align="center"></td>
                <td width="461" align="center" valign="top"><div id="firma"></div>
                  ______________________________</td>
              </tr>
              <tr>
                <td width="374" valign="top" align="center"><strong>ARQ. JOSE ANTONIO GARCIA MORENO</strong><strong><br />
                  Presidente </strong></td>
                <td width="185" valign="top" align="center">&nbsp;</td>
                <td width="461" align="center" valign="top"><strong>
                  <? echo $header['razon']?>
                </strong></td>
              </tr>
              <tr>
                <td width="374" valign="top" align="center">&nbsp;</td>
                <td width="185" valign="top" align="center">&nbsp;</td>
                <td width="461" valign="top" align="center"><strong>
                  <? echo $header['ci_rif']?>
                </strong></td>
              </tr>
              <tr>
                <td width="374" valign="top" align="center">Resolución Nº. DA/406/2015 de fecha 02/10/2015, <br />
                  Gaceta Municipal
                  N°. 15/4346, de fecha 08/10/2015</td>
                <td width="185" valign="top" align="center">&nbsp;</td>
                <td width="461" valign="top"><p align="center"><strong>&nbsp;</strong></p></td>
              </tr>
              <tr>
                <td width="374" valign="top">&nbsp;</td>
                <td width="185" valign="top"><p align="center"><strong><em>&nbsp;</em></strong></p></td>
                <td width="461" valign="top"><p align="center"><strong><em>&nbsp;</em></strong></p></td>
              </tr>
            </table>
            <p>
            JAGM/magm.-<strong>              </strong></p>
          
          <p align="center">
            <img width="100%" height="3" src="comunicacion_de_deuda_clip_image001.gif"><br>
Urb. El Viñedo CLL.141 (Monseñor Adams) Plaza Cristóbal Mendoza Edificio Sede IMA, San José, Valencia, Estado Carabobo.
Telefax: (0241) 8234938 / 8234221 Correo: valencia@ima.gob.ve www.ima.gob.ve

			</p>
            
		</td>
        
      </tr>
    </table>
      
    </td>
    </td>
    
  </tr>
    </table>
          </p>
          </td>
      </tr>
    </table>
    
    </td>
  </tr>
</table>
</body>
<style>
@charset "utf-8";
/* CSS Document */
body{
  font-family:"Segoe UI";
}
.contenerdorFactura{
  width:900px;
  overflow:hidden;
  margin-top:100px;
}
.contenerdorFactura2{
  width:900px;
  overflow:hidden;
  margin-top:0px;
}
.contenerdorFactura3{
  width:900px;
  overflow:hidden;
  margin-top:180px;
}
.contenerdorFactura2{
  width:900px;
  overflow:hidden;
}
.cuadroTop{
  width:100%;
  overflow:hidden;
}
.cuadroTopDer{
  height:130px;
  width:65%;
  float:left;
}
.cuadroTopIzq{
  height:80px;
  width:33%;
  float:left;
  padding-left:10px;
  padding-top:40px;
  
}
.lineTop{
  width:100%;
  height:20px auto;
}
.cuadroCenter{
  width:100%;
  margin-top:10px;
  height:200px;
}
.cuadroCenterEstado{
  width:100%;
  margin-top:10px;
  height: auto ;
}
.lineCemter{  
  width:100%;
  height:20px;
}
.item{
  width:70%;
  float:left;
}
.monto{
  width:27%;
  float:left;
  margin-left:1%
}
.lineSeparador{
  width:100%;
  border-top:2px #000000 solid;
  margin-top:5px;
}
.lineSeparador2{
  width:100%;
  border-top:2px #000000 solid;
  margin-top:5px;
  overflow:hidden;
}
.cuadroFooter{
  width:100%;
  overflow:hidden;
}
.cuadroTotales{
  width:30%;
  float:right;
}
.cuadroFormas{
  width:68%;
  float:left;
}
.lineTotales{
  width:100%;
  border:#FF0;
  overflow:hidden;
}
.totalesIzq{
  width:47%;
  padding-right:5px;
  float:left;
  text-align: right;
}
.totalesDer{
  width:47%;
  padding-left:5px;
  float:left;
  text-align: left;
}
.tituloEstado{
  border-top:1px #000000 solid;
  border-bottom:1px #000000 solid;
  text-align:center;
  font-size:18px;
}
#forma{
  width:auto;
  float:left;
  margin-right:3px;
  font-size:12px;
}
#dirfac{
  font-size:12px;
}
 </style> 
</html>
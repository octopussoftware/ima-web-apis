<?php

include('./connect.php');
include('./funciones_gen.php');
$id_inm = $_POST['id_inm'];
$id_doc = $_POST['id_doc'];

$sql="select c.id,c.cod_ima,c.ci_rif,c.razon, p.parr , z.zona, e.edif, i.av_calle,i.no_inmueble, i.piso
from contribuyente c
inner join inmuebles i on (i.id_contribuyente = c.id)
inner join parroquias p on (p.id = i.id_parroquia)
inner join zonas z on (z.id = i.id_zona)
inner join edif e on (e.id = i.id_edif)
where i.id = $id_inm
";

$result = $conn->query($sql);
$data = $result->fetch_assoc();  

// print_r($data);

$av_calle=$data['av_calle'];
$no_inmueble=$data['no_inmueble'];
$piso=$data['piso'];

$dir_ubi = "Parroquia ".$data['parr'].", Sector ".$data['zona'].", ".$data['edif'];

if (!is_null($av_calle)) $dir_ubi=$dir_ubi.", Av / Calle $av_calle";
if (!is_null($no_inmueble)) $dir_ubi=$dir_ubi.", $no_inmueble";
if (!is_null($piso)) $dir_ubi=$dir_ubi.", Piso $piso";

$curr = date('Y-m-d H:i:s', time());

//print_r($curr);

if ($id_doc == '') $id_doc = 0;

$sql="select count(*) as cuantos from documentos where id = $id_doc";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();

//print_r($row);

if ($row['cuantos'] == 0 ) {

$referencia = generardoc('sol');

$num_cert = $referencia;
         
$sql="insert into documentos (tipo,referencia,id_pagos,fecha_crea,fecha_mod,fecha_doc,control,monto) values ('sol','$referencia',$id_inm,'$curr','$curr','$curr','N/A',0)";
$conn->query($sql);

} else {
$sql="select * from documentos where id = $id_doc";
$rs=$conn->query($sql);
$row = $rs->fetch_assoc();

$referencia = $row['id'];
$num_cert = $referencia;

}
//Root server
$server = 'http://ima.gob.ve';

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>solvencia</title>
<link href="../css/style.css" rel="stylesheet" type="text/css">
<style>
#firma{
    width:230px;
    height:69px;
    z-index:2;
    position: static;
    margin-bottom:-13px;
    margin-left:60px;
}
</style>
</head>

<body>
<table width="600"  class="centro" border="0" cellspacing="0" align="center">
<div align="center"><img src="<? echo $server ?>/assets/img/docs/solv/ima-sol.jpg" width="900"  /></div>
  <tr>
    <td align="center" width="600"><div  class="" align="justify" >
      <div class="menbrete2">
         <div class="imgmenbrete"  align="right"></div>
</div>
      <div style="text-align: center; font-weight: bold;">CERTIFICADO N°: <? echo $num_cert ?></div>
      <table width="" border="0" cellspacing="0">
      <tr>
        <td><table width="580" border="0" cellspacing="0">
          <tr>
            <td width="421"><table width="" border="0" cellspacing="0">
              <tr>
                <td><table width="570" border="0" cellspacing="0">
                                   <tr>
                    <td width="153">Código:</td>
                    <td width="413"><? echo $data['id'] ?></td>
                  </tr>
                  <tr>
                    <td>Razón Social/Nombre:</td>
                    <td><? echo $data['razon'] ?></td>
                  </tr>
                  <tr>
                    <td>RIF / CI:</td>
                    <td><? echo $data['ci_rif'] ?></td>
                  </tr>
                                    <tr>
                  </tr>
                                    <tr>
                    <td>Datos del Inmueble:</td>
                    <td><? echo $dir_ubi ?></td>
                  </tr>
                  <tr>
                    <td>Fecha de Emisión:  </td>
                    <td><? echo $curr?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      </table>
      <div style="font-weight: bold"> El presente Certificado de Solvencia es válido únicamente durante el mes de su emisión.</div>
    </div>
      <table border="0" cellspacing="0">
        <tr>
          <td><span style="text-align: left"><img src="<? echo $server ?>/assets/img/docs/solv/qr.png" width="100" height="100"  /></span></td>
          <td style="text-align: center">
          <div id="firma"><img src="<? echo $server ?>/assets/img/docs/solv/joel.png" width="230" height="69" /></div>
          <strong>ARQ. JOSE ANTONIO GARCIA MORENO</strong><br>
            <span style="font-weight: bold">PRESIDENTE</span><br>
Instituto Municipal del Ambiente (IMA)<br>
Resolución Nº DA/406/2015 de fecha 02/10/2015<br>
Gaceta Municipal de Valencia Nº 15/4346, de fecha 08/10/2015</td>
        </tr>
      </table>
      <hr />    
     <div class="floor"> <p align="center"><strong>Oficina:</strong> Urb. El Viñedo CLL.141 (Monseñor Adams) Plaza Cristóbal Mendoza Edificio Sede IMA, San José, Valencia. www.ima.gob.ve<br>
   
    <strong>Teléfonos:</strong>&nbsp;58 - (0241) 8234938 / 8234221 Fax: (0241) 858.78.07. / 858.01.72.</p></div></td>
  </tr>
</table>


</body>

<style>
   @charset "utf-8";

.centro{
    font-size: 12px;
    text-align: left;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}

.menbrete{
    background-color:#3BFF17;
    color:#FFFFFF;
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}
.menbrete2{
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}

.imgmenbrete{
    float:right;
    z-index:2;
}

.floor{
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:9px;
}


</style>                    

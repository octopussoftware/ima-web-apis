<?php
include('./connect.php');
include('./funciones_gen.php');

$id_cuota = $_POST['id_cuota'];
$id_inm = $_POST['id_inm'];
$monto = $_POST['amount'];
$id_cajero = $_POST['id_cajero'];
$id_tipo_pago = $_POST['id_tipo_pago'];
$ag_ret = $_POST['ag_ret']; 


$sql="select * from x_convenio where id = $id_cuota";
$result = $conn->query($sql);
$xconv = $result->fetch_assoc();

$fecha_vence=$xconv['fecha_vence'];
$curr = date('Y-m-d');



// print_r($xconv);

$monto_cuota = number_format($xconv['monto_cuota'],2,'.','');

$total_fact = number_format($monto_cuota*1.12,2,'.','');
$monto_sin_iva = number_format($monto_cuota,2,'.','');
$iva_fact = number_format($monto_cuota*0.12,2,'.','');



if ($monto == 'ret') {

    //$ag_ret = 1;

    $sql="select iva from pagos where tipo_pago = 'conv' and id_doc = $id_cuota";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

    $monto = number_format($row['iva']*0.75,2,'.','');
} 


$sql="select * from pagos where tipo_pago='conv' and id_doc=$id_cuota";
$result = $conn->query($sql);
$row_pag = $result->fetch_assoc();

if (count($row_pag) == 0) {

    $multa = 0;

     if ($curr > $fecha_vence) {

        $multa = 30*300;
     }

$total_fact_multa = number_format($total_fact + $multa,2,'.','');

// $sql="insert into pagos (id_inm,monto,estatus,tipo_pago,id_doc) values ($id_inm,$monto_cuota,'nuevo','conv',$id_cuota)";
// $conn->query($sql);

$sql="insert into pagos (id_inm,monto,monto_sin_iva,iva,estatus,tipo_pago,id_doc,monto_multa) values ($id_inm,$total_fact_multa,$monto_sin_iva,$iva_fact,'cargar_pagos','conv',$id_cuota,$multa)";
$conn->query($sql);


$sql = "SELECT LAST_INSERT_ID()";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$id_pagos = $row['LAST_INSERT_ID()'];

// $pagado=0;
$sql="insert into d_pagos (id_pagos,periodo,descripcion,monto) values ($id_pagos,'convenio','monto_cuota',$total_fact)";
$conn->query($sql);

if ($multa != 0) {
$sql="insert into d_pagos (id_pagos,periodo,descripcion,monto) values ($id_pagos,'convenio','multa',$multa)";
$conn->query($sql);
}

} else {

$id_pagos = $row_pag['id'];
$multa = $row_pag['monto_multa'];

}



// echo $id_pagos;

// exit;

$sql="select count(*) as cuantos from x_pagos where id_pagos = $id_pagos";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$cuantos = $row['cuantos'];

if ($cuantos == 0) {

$pagado = 0;
$monto_total=$total_fact + $multa;

} else {

$sql="select sum(x.monto) as pagado,p.monto from x_pagos x
inner join pagos p on (p.id = x.id_pagos)
where x.id_pagos = $id_pagos group by p.monto";
// echo $sql;
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$pagado = $row['pagado'];
$monto_total = $row['monto'] + $multa;

}

$faltan = number_format($monto_total - $pagado, 2, '.', '');  



//echo "pagado ---   $pagado   -------   monto_total ------   $monto_total   ------  faltan --- $faltan".PHP_EOL;

if ($monto > $faltan ) {
 //if (true ) {   
//ya esta pago o el monto es mayor al faltante
    $resp["error"] = true;
    $resp["data"] = 'amount_higher_than_saldo';
    //$resp["data"] = "pagado --- $pagado --- monto_total --- $monto_total --- faltan --- $faltan";
    echo json_encode($resp);
    exit;
}

if (isset($_POST['com'])) {
    $comentario = $_POST['com'];
} else {
    $comentario = '';
}


// exit;




$fecha_reg = date("Y/m/d");
$fecha_mod = date("Y/m/d");

//Si el cajero es 5 (Cajero Web) y tipo pago es 5/6 es un reporte de pago
if($id_cajero == 5 && ($id_tipo_pago == 5 || $id_tipo_pago == 6))
    $estatus = 'reportado';
else
    $estatus = 'aprobado';

$saldo = (float)($faltan - $monto);

switch ($id_tipo_pago)
{
	case '1':   //efectivo
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario,saldo) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', $saldo)";
    break;

    
    case '2':   //cheque
    $referencia = $_POST['referencia'];
    $id_banco_out = $_POST['id_banco_out'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_out,saldo,fecha_doc) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_out',$saldo,'$fecha_doc')";
    break;
 
    case '3':   //debito
    $referencia = $_POST['referencia'];
    $id_banco_out = $_POST['id_banco_out'];
    $id_banco_in = $_POST['id_banco_in'];
    $lote = $_POST['lote'];
    $aprobacion = $_POST['aprobacion'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_out,id_banco_in,lote,aprobacion,saldo) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_out', '$id_banco_in', '$lote', '$aprobacion',$saldo)";
    break;

    case '4':   //credito
    $referencia = $_POST['referencia'];
    $origen = $_POST['origen'];
    $aprobacion = $_POST['aprobacion'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia,origen,aprobacion,saldo) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$origen', '$aprobacion',$saldo)";
    break;

    case '5' :   //transferencia 
    $referencia = $_POST['referencia'];
    $id_banco_out = $_POST['id_banco_out'];
    $id_banco_in = $_POST['id_banco_in'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_out,id_banco_in,saldo,fecha_doc) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_out', '$id_banco_in', $saldo,'$fecha_doc')";
    break;

    case '6' :   //deposito
    $referencia = $_POST['referencia'];
    $id_banco_in = $_POST['id_banco_in'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, id_banco_in,saldo,fecha_doc) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', '$id_banco_in', $saldo,'$fecha_doc')";
    break;

    case '75' :   //retencion
    $referencia = $_POST['referencia'];
    $fecha_doc = $_POST['fecha_doc'];
    $sql = "insert into x_pagos (id_pagos, id_tipo_pago, monto, id_cajero, fecha_reg, fecha_mod, estatus, comentario, referencia, saldo,fecha_doc) values ($id_pagos, $id_tipo_pago, $monto, $id_cajero, '$fecha_reg', '$fecha_mod', '$estatus', '$comentario', '$referencia', $saldo,'$fecha_doc')";
    // echo $sql;
    break;

   
}


// echo $sql.PHP_EOL;

if ($conn->query($sql) === TRUE) {

$sql = "SELECT LAST_INSERT_ID()";
$result = $conn->query($sql);
$row = $result->fetch_assoc();


$estatus = 'cargar_pagos';

if ($saldo == 0 && $ag_ret == 0) {
    if(chequearReportePagoWeb($id_pagos) == 0)
        $estatus = 'imp_fact';
    else
        $estatus = 'reportado';
}

if ($saldo == 0 && $ag_ret == 1) {
    if(chequearReportePagoWeb($id_pagos) == 0)
        $estatus = 'terminado';    
    else
        $estatus = 'reportado';
}

$sql="update x_convenio set id_pagos = $id_pagos, estatus = 'pagada' where id = $id_cuota";
$conn->query($sql);

$sql="update pagos set estatus = '$estatus' where id = $id_pagos";
$conn->query($sql);

$id = $row['LAST_INSERT_ID()'];
$sql = "select * from x_pagos where id = $id";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$resp["error"] = false;
$resp["data"] = $row;


} else {
  $resp["error"] = true;
  $resp["msg_error"] =  $conn->error;
}


// print_r($resp);
echo json_encode($resp);
?>

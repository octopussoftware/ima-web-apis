<?php  

error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'vendor/autoload.php';  

use Illuminate\Database\Capsule\Manager as Capsule; 

$db = new Capsule;

$db->addConnection(array(
	'driver'    => 'mysql',
	'host'      => 'localhost',
	'database'  => 'ima_web',
	'username'  => 'tuity',
	'password'  => '#mysql_2016',
	'charset'   => 'utf8mb4',
	'collation' => 'utf8mb4_spanish_ci',
	'prefix'    => ''
	));

//available in static methods
$db->setAsGlobal();

$db->bootEloquent();

//Date
date_default_timezone_set('America/Caracas');


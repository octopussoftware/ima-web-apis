<?php
include('./connect.php');
$id_inm = $_POST['id_inm'];
$id_pagos = $_POST['id_pag'];
$id_doc = $_POST['id_doc'];
$tipo_pago = $_POST['tipo_pago'];

$curr = date('Y-m-d');

$sql="select * from documentos where tipo='rec' and id_pagos=$id_pagos";  //se verifica que la factura se creo
$result = $conn->query($sql);
$row_pag = $result->fetch_assoc();

$referencia = $row_pag['referencia'];

//verificar cuando se llama sin enviar id_pagos
$sql="select p.id,c.razon,c.ci_rif,c.dir,c.telf,p.fecha_fact,nro_factura,i.id id_inm,p.monto from pagos p 
inner join inmuebles i on (i.id = p.id_inm)
inner join contribuyente c on (c.id = i.id_contribuyente)
where p.id = $id_pagos";

$result = $conn->query($sql);
$header = $result->fetch_assoc();  

$sql="select i.id,tar.descripcion,parr.parr,z.zona,i.av_calle, e.edif, i.no_inmueble, i.piso, i.mts from inmuebles i 
inner join parroquias parr on (parr.id = i.id_parroquia)
inner join zonas z on (z.id = i.id_zona)
inner join edif e on (e.id = i.id_edif)
left join tarifas_com tar on (tar.id = i.id_actividad)
where i.id = $id_inm";//direccion de inmueble

$result = $conn->query($sql);
$inmdir = $result->fetch_assoc();

if ($inmdir['descripcion'] == '') {
	$tipo = "Residencial, Mts.: ".$inmdir['mts'];
} else {
	$tipo = "Comercial / ".$inmdir['descripcion'].", Mts.: ".$inmdir['mts'];
}

$av_calle=$inmdir['av_calle'];
$no_inmueble=$inmdir['no_inmueble'];
$piso=$inmdir['piso'];

$dir_ubi = "Parroquia ".$inmdir['parr'].", Sector ".$inmdir['zona'].", ".$inmdir['edif'];

if (!is_null($av_calle)) $dir_ubi=$dir_ubi.", Av / Calle $av_calle";
if (!is_null($no_inmueble)) $dir_ubi=$dir_ubi.", $no_inmueble";
if (!is_null($piso)) $dir_ubi=$dir_ubi.", Piso $piso";

switch ($tipo_pago)
{
    case 'aseo' :
    $detalle = "Pago de multa correspondiente a morosidad en el servicio de recoleccion y disposicion";
    break;
    case 'conv' :
    $detalle = "Pago de multa correspondiente al atraso en cuotas de convenio";
    break;
}

//Root server
$server = 'http://ima.gob.ve';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>RECIBO DE PAGO IMA</title>
<!-- <link href="assets/css/fact-styles/factura.css" rel="stylesheet" type="text/css" /> -->
</head>
<body onload="window.print()">
<div class="contenerdorFactura">
	<div class="cuadroTop">
    	<div class="cuadroTopDer">
        	<div class="lineTop"><b>Razón Social/Nombre: </b><? echo $header['razon']?></div>
            <div class="lineTop">
            	<b>RIF / CI: </b><? echo $header['ci_rif']?>                            </div>
            <div class="lineTop">
            	<b>Dirección Fiscal: </b><? echo $header['dir']?>
                <span id="dirfac"></span>
            </div>
            <div class="lineTop">
            	<b>Tipo de Inmueble: </b><? echo $tipo?></br>
            	<b>Datos del Inmueble: </b><? echo $dir_ubi?>
            	<span id="dirfac"></span>
            </div>
        </div>
        <div class="cuadroTopIzq">
        	<div class="lineTop"><b>N° Recibo: <? echo $referencia?><br> 
        	Serie </b></div>
            <div class="lineTop"><b>Fecha de Emisión: </b><? echo $curr?></div>
                        <div class="lineTop"><b>Usuario: </b>Cajero Web </b></div>
                    </div>
    </div>
    <div class="cuadroCenter">
    	<div class="lineCemter">
        	<div class="item">
        	  <div align="center"><B>CONCEPTO</B></div></div>
            <div class="monto">
              <div align="center"><B>TOTAL</B></div></div>
        </div>    
        <div class="lineSeparador"></div>
                <div class="lineCemter">
        	<div class="item">
            </br>
            <div align="left"><? echo $detalle?></div>
            </div>
            </br>
            <div class="monto" align="right"><div align="right"><? echo number_format($row_pag['monto'],2,',','.')?></div></div>
        </div> 
    </div>
    <div class="lineSeparador"></div>
    <div class="cuadroFooter">
    <div class="cuadroFormas">
    			<div></div>
              <div id="forma"><b></b></div>
                </div>
    	<div class="cuadroTotales">
        	<div class="lineTotales">
            	<div class="totalesIzq"><b>TOTAL  Bs.</b></div>
                <div class="totalesDer"><div align="right"><? echo number_format($monto_sin_iva,2,',','.')?></div></div>            	
            </div>
        </div>    
    </div>
</div>
</body>  

<style>
    @charset "utf-8";
/* CSS Document */
body{
    font-family:"Segoe UI";
}
.contenerdorFactura{
    width:900px;
    overflow:hidden;
    margin-top:100px;
}
.contenerdorFactura2{
    width:900px;
    overflow:hidden;
    margin-top:0px;
}
.contenerdorFactura3{
    width:900px;
    overflow:hidden;
    margin-top:180px;
}
.contenerdorFactura2{
    width:900px;
    overflow:hidden;
}
.cuadroTop{
    width:100%;
    overflow:hidden;
}
.cuadroTopDer{
    height:130px;
    width:65%;
    float:left;
}
.cuadroTopIzq{
    height:80px;
    width:33%;
    float:left;
    padding-left:10px;
    padding-top:40px;
    
}
.lineTop{
    width:100%;
    height:20px auto;
}
.cuadroCenter{
    width:100%;
    margin-top:10px;
    height:200px;
}
.cuadroCenterEstado{
    width:100%;
    margin-top:10px;
    height: auto ;
}
.lineCemter{    
    width:100%;
    height:20px;
}
.item{
    width:91%;
    float:left;
}
.monto{
    width:6%;
    float:left;
    margin-left:1%
}
.lineSeparador{
    width:100%;
    border-top:2px #000000 solid;
    margin-top:5px;
}
.lineSeparador2{
    width:100%;
    border-top:2px #000000 solid;
    margin-top:5px;
    overflow:hidden;
}
.cuadroFooter{
    width:100%;
    overflow:hidden;
}
.cuadroTotales{
    width:30%;
    float:right;
}
.cuadroFormas{
    width:68%;
    float:left;
}
.lineTotales{
    width:100%;
    border:#FF0;
    overflow:hidden;
}
.totalesIzq{
    width:47%;
    padding-right:5px;
    float:left;
    text-align: right;
}
.totalesDer{
    width:47%;
    padding-left:5px;
    float:left;
    text-align: left;
}
.tituloEstado{
    border-top:1px #000000 solid;
    border-bottom:1px #000000 solid;
    text-align:center;
    font-size:18px;
}
#forma{
    width:auto;
    float:left;
    margin-right:3px;
    font-size:12px;
}
#dirfac{
    font-size:12px;
}

.centro{
    font-size: 12px;
    text-align: left;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}

.menbrete{
    background-color:#3BFF17;
    color:#FFFFFF;
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}
.menbrete2{
    padding-left:20px;
    padding-top:10px;
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:14px;
    
}

.imgmenbrete{
    float:right;
    z-index:2;
}

.floor{
    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    font-size:9px;
}


</style>                    

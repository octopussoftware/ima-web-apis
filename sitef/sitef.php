<?php

date_default_timezone_set('America/Caracas');

include_once('nusoap.php');
include_once('credentials.php');

class SitefPayment
{

  public $sitefData;
  public $cardnumber;
  public $cardtype;
  public $cardname;
  public $cardci;
  public $carddate;
  public $cardccv;
  public $cardamount;
  public $endpoint;
  public $merchantId;

  function __construct(){
    global $credentials;

    $this->sitefData = $credentials;
    $this->storeusn = $this->sitefData['storeUSN'];
    $this->endpoint = $this->sitefData['sandbox'] ? $this->sitefData['sandboxData']['endpoint'] : $this->sitefData['prodData']['endpoint'];
    $this->merchantId = $this->sitefData['sandbox'] ? $this->sitefData['sandboxData']['merchantId'] : $this->sitefData['prodData']['merchantId'];
    $this->merchantKey = $this->sitefData['sandbox'] ? $this->sitefData['sandboxData']['merchantKey'] : $this->sitefData['prodData']['merchantKey'];

  }

  public function beginTransaction($amount, $orderid){

    $wsdl = true;
    $proxyhost = false;
    $proxyport = false;
    $proxyusername = false;
    $proxypassword = false;
    $timeout = 0;
    $response_timeout = 300;
    $client = new nusoap_client($this->endpoint, $wsdl, $proxyhost, $proxyport, $proxyusername, $proxypassword, $timeout, $response_timeout);
    $err = $client->getError();

    if ($err) 
      return array (
        'error' => true,
        'message' => 'Error al intentar el cliente nusoap'
        );
  
    $this->cardamount = $amount;
    $this->orderid = $orderid; 

    $transactionRequest= array('transactionRequest' => array(
        'amount' => $this->cardamount,
        'merchantId' => $this->merchantId,
        'merchantUSN' => $this->storeusn,
        'orderId' => $this->orderid,
        ));

    $payment = $client->getProxy();
    $transactionResponse = $payment->beginTransaction($transactionRequest);   //aqui llamo

    if(!isset($transactionResponse['transactionResponse']['nit']))
      return array (
        'error' => true,
        'message' => 'No existe NIT en la respuesta'
        );

    $nit = $transactionResponse['transactionResponse']['nit'];

    return array ('nit' => $nit);
  }

  public function doPayment($nit, $fnumber, $ftype, $fname, $fci, $fdate, $fccv) {

    $wsdl = true;
    $proxyhost = false;
    $proxyport = false;
    $proxyusername = false;
    $proxypassword = false;
    $timeout = 0;
    $response_timeout = 300;
    $client = new nusoap_client($this->endpoint, $wsdl, $proxyhost, $proxyport,$proxyusername, $proxypassword, $timeout, $response_timeout);
    $err = $client->getError();

    if ($err) 
      return array (
        'error' => true,
        'message' => 'Error al intentar el cliente nusoap'
        );

    $this->cardnumber = $fnumber;
    $this->cardtype = $ftype;
    $this->cardname = $fname;
    $this->cardci = $fci;
    $this->carddate = $fdate;
    $this->cardccv = $fccv;
    $this->nit = $nit;

    $paymentRequest = array('paymentRequest' => array (
      'authorizerId' => $this->cardtype,
      'autoConfirmation' => 'true',
      'cardExpiryDate' => $this->carddate,
      'cardNumber' => $this->cardnumber,
      'cardSecurityCode' => $this->cardccv,
      'customerId' => $this->cardci,
      'installmentType' => '4',
      'installments' => '1',
      'nit' => $this->nit
      ));

    $payment = $client->getProxy();
    $result = $payment->doPayment($paymentRequest);

    if ($client->fault)
        return array (
          'error' => true,
          'message' => 'Error client fault L77'
        );

    $err = $client->getError();

    if ($err) 
      return array (
        'error' => true,
        'message' => 'Error client fault L85'
        );

    $status = $result['paymentResponse']['transactionStatus'];

    if ($status === 'CON')
      return $this->paymentOk($result, $nit);
    
    return $this->paymentError($result, $nit);
  }

  public function getStatus($nit){

    $wsdl = true;
    $proxyhost = false;
    $proxyport = false;
    $proxyusername = false;
    $proxypassword = false;
    $timeout = 0;
    $response_timeout = 300;

    $client = new nusoap_client($this->endpoint, $wsdl, $proxyhost, $proxyport,$proxyusername, $proxypassword, $timeout, $response_timeout);

    $err = $client->getError();

    if ($err) 
      return array (
        'error' => true,
        'message' => 'Error client fault L85'
        );

    $getStatusRequest = array('merchantKey' => $this->merchantKey, 'nit' => $nit);

    $payment = $client->getProxy();
    $result = $payment->getStatus($getStatusRequest);  

    $status = $result['paymentResponse']['transactionStatus'];

    if ($status === 'CON')
      return $this->paymentOk($result, $nit);

    return $this->paymentError($result, $nit);
  }

  public static function paymentOk($result, $nit){

    $responsecode = $result['paymentResponse']['sitefResponseCode'];
    $esitefusn = $result['paymentResponse']['esitefUSN'];
    $sitefusn = $result['paymentResponse']['sitefUSN'];
    $orderid = $result['paymentResponse']['orderId'];
    $message = $result['paymentResponse']['message'];
    $customer_tck = $result['paymentResponse']['customerReceipt'];
    $merchant_tck = $result['paymentResponse']['merchantReceipt'];

    return array (
      'error' => false,
      'data' => array(
        'posSource' => 'sitef',
        'nit' => $nit,
        'status' => 'OK',
        'esitefusn' => $esitefusn,
        'sitefusn' => $sitefusn,
        'orderid' => $orderid,
        'message' => $message
      ));
  }

  public static function paymentError($result, $nit){

    $responsecode = $result['paymentResponse']['responseCode'];
    $message = $result['paymentResponse']['message'];

    switch ($responsecode) {
      case 1:    $tablecode = "Solicitud nula o vacio"; break;
      case 2:    $tablecode = "Campo nit es nulo"; break;
      case 3:    $tablecode = "Campo nit es invalido"; break;
      case 4:    $tablecode = "Campo authorizerId es nulo o negativo"; break;
      case 5:    $tablecode = "Campo authorizerId es invalido"; break;
      case 6:    $tablecode = "Campo autoConfirmation es nulo"; break;
      case 7:    $tablecode = "Campo cardExpiryDate es nulo"; break;
      case 8:    $tablecode = "Campo cardExpiryDate es invalido (fecha invalida)"; break;
      case 9:    $tablecode = "Campo cardExpiryDate ha expirado"; break;
      case 10:   $tablecode = "Campo cardExpiryDate es invalido (no numerico)"; break;
      case 11:   $tablecode = "Campo cardExpiryDate es invalido (violo tama�o)"; break;
      case 12:   $tablecode = "Campo cardNumber es nulo"; break;
      case 13:   $tablecode = "Campo cardNumber es invalido (no numerico o negativo)"; break;
      case 14:   $tablecode = "Campo cardNumber es invalido"; break;
      case 15:   $tablecode = "Campo cardNumber es invalido"; break;
      case 16:   $tablecode = "Campo cardSecurityCode es nulo"; break;
      case 17:   $tablecode = "Campo cardSecurityCode es invalido (no numerica,negativo o mayor que 5 digitos)"; break;
      case 18:   $tablecode = "Campo installments es nulo o negativo"; break;
      case 19:   $tablecode = "Campo installmentType es nulo o invalido"; break;
      case 21:   $tablecode = "Campo customerId es no numerico, negativo o el tama�o ha sido violado"; break;
      case 26:   $tablecode = "Cambio de autorizador fallo."; break;
      case 27:   $tablecode = "Cambio de autorizador bloqueado."; break;
      case 30:   $tablecode = "La transacci�n ha terminado. Reinicie el flujo"; break;
      case 98:   $tablecode = "Operaci�n de pago fallo"; break;
      case 99:   $tablecode = "Confirmaci�n de pago fallo"; break;
      case 131:  $tablecode = "Error de comunicacion del Servidor Sitef"; break;
      case 255:  $tablecode = "Pago denegado, Tarjeta denegada"; break;
      case 1000: $tablecode = "Error interno inesperado de e-Sitef. Por favor, pongase en contacto con soporte."; break;
      case 1003: $tablecode = "Error interno inesperado de e-Sitef. Por favor, pongase en contacto con soporte."; break;
      case 1004: $tablecode = "Error interno inesperado de e-Sitef. Por favor, pongase en contacto con soporte."; break;
      case 2000: $tablecode = "Error interno inesperado de e-Sitef. Por favor, pongase en contacto con soporte."; break;
      case 5555: $tablecode = "Error interno inesperado de e-Sitef. Por favor, pongase en contacto con soporte."; break;
    }

    return array (
     'error' => true,
     'data' => array(
       'nit' => $nit,
       'errorcode' => $responsecode,
       'message' => $message,
       'tablecode' => $tablecode));
  }

}
?>



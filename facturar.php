<?php
include('./connect.php');
include('./funciones_gen.php');

$id_pagos = $_POST['id_pagos'];
$id_inm = $_POST['id_inm'];
$ag_ret = $_POST['ag_ret'];

//verificar cuando se llama sin enviar id_pagos
if (!$id_pagos) {
	$sql="select * from pagos where id_inm = $id_inm and estatus = 'nuevo'";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
	$id_pagos = $row['id'];
}

//New payment dates to return to client for status change purposes
$sql="select * from d_pagos where id_pagos = $id_pagos and descripcion = 'reco' order by id_d_pagos desc limit 1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$last_periodo = $row['periodo'];

$last_vec = explode("-",$last_periodo);

$new_mes_hasta = str_pad($last_vec[0], 2, '0', STR_PAD_LEFT);
$new_ano_hasta = $last_vec[1];


$sql = "update inmuebles set mes_hasta = '$new_mes_hasta', ano_hasta = '$new_ano_hasta' where id = $id_inm";
$conn->query($sql);

if ($ag_ret === '1') {
	$sql = "update pagos set estatus = 'cargar_ret' where id = $id_pagos";
	if ($conn->query($sql) === TRUE) {
		$resp["error"] = false; 
		$resp['data'] = array('estatus' => 'cargar_ret', 'id_pagos' => $id_pagos);
	}else{
	  $resp["error"] = true;
	  $resp["data"] =  $conn->error;
	}
} else {
	$sql = "update pagos set estatus = 'terminado' where id = $id_pagos";
	if ($conn->query($sql) === TRUE) {
		$ano_act = date('Y');
		$mes_act = date('m');

		$ano_dif = $ano_act - $new_ano_hasta;
		$mes_dif = $mes_act - $new_mes_hasta;

		if ($ano_dif == 0) {
			if ($mes_dif > 1) {
				$estatus = "no_solvente";
			} else {
				$estatus = "solvente";
			}
		} else {
			$estatus = "no_solvente";
		}

		$resp["error"] = false; 
		$resp['data'] = array('estatus' => $estatus, 'id_pagos' => $id_pagos);
	}else{
	  $resp["error"] = true;
	  $resp["data"] =  $conn->error;
	}
}

echo json_encode($resp);
                         
?>

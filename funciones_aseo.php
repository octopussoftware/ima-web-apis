<?php
include('./connect.php');


function generarInteresesA($monto, $ano, $mes){
	include('./connect.php');

	$iy = $ano;
	$fy = date("Y");
	$meses[01]='Enero';
	$meses[1]='Enero';
	$meses[02]='Febrero';
	$meses[2]='Febrero';
	$meses[03]='Marzo';
	$meses[3]='Marzo';
	$meses[04]='Abril';
	$meses[05]='Mayo';
	$meses[5]='Mayo';
	$meses[06]='Junio';
	$meses[6]='Junio';
	$meses[07]='Julio';
	$meses[7]='Julio';
	$meses[08]='Agosto';
	$meses[8]='Agosto';
	$meses[09]='Septiembre';
	$meses[9]='Septiembre';
	$meses[10]='Octubre';
	$meses[11]='Noviembre';
	$meses[12]='Diciembre';
	$sql="SELECT * FROM `aeintereses`";	
	$query = $conn->query($sql);
	// $query=mysql_query($sql);
	// while($dato=mysql_fetch_assoc($query)){
	while($dato = $query->fetch_assoc()){
		// print_r($dato);
		$tasa[$dato['year']][$dato['mes']]=$dato["tasa"];
	}
	$tr=0;
	for($i=$iy;$i<=$fy;$i++){
		if($iy==$i&&$fy!=$i){ $im=$mes; $fm=12;}
		if($fy==$i&&$iy!=$i){ $im=1; $fm=date("m");}
		if($fy==$i&&$iy==$i){$im=$mes; $fm=date("m");}
		if($fy!=$i&&$iy!=$i){$im=1; $fm=12;}
		for($a=$im;$a<=$fm;$a++){
			//echo PHP_EOL.$tr."aa  $a   im    $im  fm   $fm   zzzzzzzzzzzzzzzzz".PHP_EOL;
			$tr=$tr+$tasa[$i][$meses[$a]];
		}
	}
	$interes=(($monto/100)*$tr);
	return $interes;
}


function monthsApart($oldDate,$newDate) {
	// Fecha debe ir Año-mes-dia (Y-m-d // 2015-06-06)
	list($oldYear,$oldMonth,$oldDay) = split("-",$oldDate);
	list($newYear,$newMonth,$newDay) = split("-",$newDate);
	$diff = (($newDay >= $oldDay ? 0 : -1) + ($newYear - $oldYear)* 12) +
	$newMonth - $oldMonth;
		return abs($diff);
}

class calculo{
    var $fa = 2.629;
    var $fa2 = 2.307;
        // Factor de Ajuste


    function residencial($tarifa12, $ut, $mes = 0, $ano = 0, $tarifa15 = 0, $cantidad = 1){
        $tarifa = $tarifa12;
        $mes = str_pad($mes, 2, '0', STR_PAD_LEFT);
        $fecha = intval($ano . $mes);
        
        if ($fecha >= 201512) {
            $tarifa = $tarifa15;
        }
        if ($fecha >= 201603){
            $tarifa = $tarifa15 * $this->fa;
        }
        if ($fecha >= 201703){
            $tarifa = $tarifa15 * $this->fa * $this->fa2;
        }
		$monto = $tarifa * $ut * 1;    //revisar que originalmente multiplicaba por cantidad
		return round($monto, 2);
	}

	function basico($mts, $area, $minima, $tarifa, $ut){
		
		if($mts>$area){
			$m1=$ut*$minima;
			$m1=number_format($m1, 2, '.', '');
			$m2=(($mts-$area)*$tarifa)*$ut;
			$m2=number_format($m2, 2, '.', '');
			$msj = "<p>".$m1." ".$m2." - mts ".$mts." area ".$area."<p>";
			$monto=$m1+$m2;
		}else{
			$monto=$ut*$minima;
			$monto=number_format($monto, 2, '.', '');
		}
		return $monto;		
	}

	function estanco($tmta, $aen, $am, $ut = 150){
		//$ut = 150;
		// echo "Estanco -->    $tmta, $aen, $am, $ut".PHP_EOL;
		$k = ($aen/$am);
		//$k = floor($k);
		// echo $k.PHP_EOL;
		if($k==0){ $k =1;}
		if($k<1&$k>0){ $tp = $tmta * $ut; }
		if($k == 1){ $tp = $tmta * $ut; }
		if($k>1&$k<=2){ 
			$pre_tp = $tmta + ((($aen/$am)-1)*($tmta*0.75));
			$tp = $pre_tp * $ut;
			$estanco = 2;
		}
		if($k>2&$k<=3){ 
			$pre_tp = (1.75*$tmta) + ((($aen/$am)-2)*($tmta*0.50));
			$tp = $pre_tp * $ut;
			$estanco = 3;
		}
		if($k>3&$k<=4){ 
			$pre_tp = (2.25*$tmta) + ((($aen/$am)-3)*($tmta*0.25));
			$tp = $pre_tp * $ut;
			$estanco = 4;
		}
		if($k>4&$k<=5){ 
			$pre_tp = (2.50*$tmta) + ((($aen/$am)-4)*($tmta*0.05));
			$tp = $pre_tp * $ut;
			$estanco = 5;
		}
		if($k>5&$k<=7){ 
			$pre_tp = (2.55*$tmta) + ((($aen/$am)-5)*($tmta*0.01));
			$tp = $pre_tp * $ut;
			$estanco = 5;
		}
		if($k > 6){ 
			$pre_tp = (2.56*$tmta);
			$tp = $pre_tp * $ut;
			$estanco = 6;
		}	
	
		return $tp;
	}

	function estanco_info($tmta, $aen, $am, $ut = 150){
		// echo "Estanco INFO -->    $tmta, $aen, $am, $ut".PHP_EOL;
		$k = ($aen/$am);
		// echo $k.PHP_EOL;
		//$k = floor($k);
		if($k==0){ $k =1;}
		if($k<1&$k>0){ $tp = $tmta * $ut; }
		if($k == 1){ $tp = $tmta * $ut; }
		if($k>1&$k<=2){ 
			$pre_tp = $tmta + ((($aen/$am)-1)*($tmta*0.75));
			$tp = $pre_tp * $ut;
			$estanco = 2;
		}
		if($k>2&$k<=3){ 
			$pre_tp = (1.75*$tmta) + ((($aen/$am)-2)*($tmta*0.50));
			$tp = $pre_tp * $ut;
			$estanco = 3;
		}
		if($k>3&$k<=4){ 
			$pre_tp = (2.25*$tmta) + ((($aen/$am)-3)*($tmta*0.25));
			$tp = $pre_tp * $ut;
			$estanco = 4;
		}
		if($k>4&$k<=5){ 
			$pre_tp = (2.50*$tmta) + ((($aen/$am)-4)*($tmta*0.05));
			$tp = $pre_tp * $ut;
			$estanco = 5;
		}
		if($k>5&$k<=7){ 
			$pre_tp = (2.55*$tmta) + ((($aen/$am)-5)*($tmta*0.01));
			$tp = $pre_tp * $ut;
			$estanco = 5;
		}
		if($k > 6){ 
			$pre_tp = (2.56*$tmta);
			$tp = $pre_tp * $ut;
			$estanco = 6;
		}	
	
		return $tp."K ".$k;
	}

	function regresaUt($mes, $ano){
			$utv = array( 
				"2009" => array ("1" => "46", "2" => "46", "3" => "55", "4" => "55", "5" => "55", "6" => "55", "7" => "55", "8" => "55", "9" => "55", "10" => "55", "11" => "55", "12" => "55", ),
				"2010" => array ("1" => "55","2" => "55", "3" => "65", "4" => "65", "5" => "65", "6" => "65", "7" => "65", "8" => "65", "9" => "65", "10" => "65", "11" => "65", "12" => "65", ), 
				"2011" => array ( "1" => "65","2" => "65", "3" => "76", "4" => "76", "5" => "76", "6" => "76", "7" => "76", "8" => "76", "9" => "76", "10" => "76", "11" => "76", "12" => "76", ),
				"2012" => array ( "1" => "76","2" => "76", "3" => "90", "4" => "90", "5" => "90", "6" => "90", "7" => "90", "8" => "90", "9" => "90", "10" => "90", "11" => "90", "12" => "90", ),
				"2013" => array ( "1" => "90","2" => "90", "3" => "107", "4" => "107", "5" => "107", "6" => "107", "7" => "107", "8" => "107", "9" => "107", "10" => "107", "11" => "107", "12" => "107", ),
				"2014" => array ( "1" => "107","2" => "107", "3" => "127", "4" => "127", "5" => "127", "6" => "127", "7" => "127", "8" => "127", "9" => "127", "10" => "127", "11" => "127", "12" => "127", ),
				"2015" => array ("1" => "127", "2" => "127", "3" => "150", "4" => "150", "5" => "150", "6" => "150", "7" => "150", "8" => "150", "9" => "150", "10" => "150", "11" => "150", "12" => "150", ),
				"2016" => array ("1" => "150", "2" => "150", "3" => "177", "4" => "177", "5" => "177", "6" => "177", "7" => "177", "8" => "177", "9" => "177", "10" => "177", "11" => "177", "12" => "177", ),
				"2017" => array ("1" => "177", "2" => "177", "3" => "300", "4" => "300", "5" => "300", "6" => "300", "7" => "300", "8" => "300", "9" => "300", "10" => "300", "11" => "300", "12" => "300", )
			);

			//print_r($utv);
			return $utv[$ano][$mes*1];
	}

	function calculoResidencial($tarifa, $mesInicio, $anoInicio, $mesFin, $anoFin, $cantidad, $tarifa15){


$ver=[];
$ver['mensaje'] = '';
$ver['monto']=0;
$detalle=[];
$detalle['monto'] = 0;

		for($i=$anoInicio;$i<=$anoFin;$i++){
			if($i==$anoInicio&&$i!=$anoFin){
				for($a=($mesInicio+1);$a<=12;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
                    $detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					// $ver['mensaje'].= 'Recoleccion Periodo '.$a.'/'.$i.'   Bs. '.$monto."\n";
					// $ver['monto']= $ver['monto']+$monto;
				//$detalle[0] = 13333;
				}
			}elseif($i==$anoFin&&$i!=$anoInicio){
				for($a=1;$a<=$mesFin;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["reco"] = $monto;
					//$detalle[0] = 13333;
					$detalle['monto'] = $detalle['monto']+$monto;
				}	
			}elseif($i==$anoFin&&$i==$anoInicio){
				for($a=($mesInicio+1);$a<=$mesFin;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					//$detalle[0] = 13333;
					// print_r($detalle);
					
				}	
			}else{
				for($a=1;$a<=12;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					//$detalle[0] = 13333;
				}	
			}
			
		}
		//$detalle[0] = 13333;
		// print_r($detalle);
		return $detalle;
	}
	function calculoResidencialDispo($tarifa, $mesInicio, $anoInicio, $mesFin, $anoFin, $cantidad, $tarifa15){
$ver=[];
$ver['mensaje'] = '';
$ver['monto']=0;
$detalle=[];
$detalle['monto'] = 0;

		for($i=$anoInicio;$i<=$anoFin;$i++){			
			if($i==$anoInicio&&$i!=$anoFin){
				for($a=($mesInicio+1);$a<=12;$a++){					
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["dispo"] = $monto;
					// $ver['mensaje'].= 'Diposicion Periodo '.$a.'/'.$i.'Bs. '.$monto."\n";
					// $ver['monto']= $ver['monto']+$monto;
					$detalle['monto'] = $detalle['monto']+$monto;
				}
			}elseif($i==$anoFin&&$i!=$anoInicio){
				for($a=1;$a<=$mesFin;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					
				}	
			}elseif($i==$anoFin&&$i==$anoInicio){
				for($a=($mesInicio+1);$a<=$mesFin;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;

				}	
			}else{
				for($a=1;$a<=12;$a++){
					$monto = $this->residencial($tarifa, $this->regresaUt($a, $i), $a, $i, $tarifa15, $cantidad);
					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					
				}	
			}
			
		}
		// print_r($detalle);
		return $detalle;
	}

	function calculoVariosNuevo($mts, $actividad, $mesInicio, $anoInicio, $mesFin, $anoFin, $dif=0, $fregistro, $actividad2016){
		$ver=[];
		$ver['monto']=0;
		$detalle=[];
		$detalle['monto'] = 0;
		$ver['mensaje'] = '';

		//echo "Entramos en la funcion";

		$a2015 = $this->regresarActividad($actividad, 2015, $fregistro, $actividad2016);
		$a2012 = $this->regresarActividad($actividad, 2012, $fregistro, $actividad2016);
		$a2008 = $this->regresarActividad($actividad, 2008, $fregistro, $actividad2016);



		for($i=$anoInicio;$i<=$anoFin;$i++){		
			if($i<2012){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"]; }	
			if($i>=2012&&$i<2016){$area=$a2012["area"]; $minima=$a2012["minima"]; $tarifa=$a2012["tarifa"]; }
			if($i>2015){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"]; }	
			if($i==$anoInicio&&$i!=$anoFin){
				
				for($a=($mesInicio+1);$a<=12;$a++){	
						if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}	
						if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }



					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$monto = number_format($monto, 2, '.', '');	
					//echo $monto.PHP_EOL;
					//echo "sss"	;

					$detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Recoleccion Período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';
					}
					$ver['mensaje'].='</div>';	
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="recoleccion" />';
					$ver['mensaje'].='<div id="linea1"><input name="aaaa[]" type="hidden" value="'.$this->estanco_info($minima,$mts, $area, $this->regresaUt($a, $i)).'" /></div>';
					$ver['monto']= $ver['monto']+$monto;
				}
				
			}elseif($i==$anoFin&&$i!=$anoInicio){

				for($a=1;$a<=$mesFin;$a++){
						if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}	
						if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));	
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$monto = number_format($monto, 2, '.', '');		

					$detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					//echo $monto.PHP_EOL;
					//echo "sss"	;	
					
					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Recoleccion Período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="recoleccion" />';
					$ver['mensaje'].='<div id="linea1"><input name="aaaa[]" type="hidden" value="'.$this->estanco_info($minima,$mts, $area, $this->regresaUt($a, $i)).'" /></div>';
					$ver['monto'] = $ver['monto']+$monto;
					
				}	
			}elseif($i==$anoFin&&$i==$anoInicio){
				for($a=($mesInicio+1);$a<=$mesFin;$a++){
						if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}	
						if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					//echo $monto;
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$monto = number_format($monto, 2, '.', '');		

					$detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					// echo $monto.PHP_EOL;
					// echo "sss"	;			
					$ver['mensaje']= '<div class="linePer"><div class="linePerText">Recoleccion Período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="recoleccion" />';
					$ver['mensaje'].='<div id="linea1"><input name="aaaa[]" type="hidden" value="'.$this->estanco_info($minima,$mts, $area, $this->regresaUt($a, $i)).'" /></div>';
					$ver['monto'] = $ver['monto']+$monto;
					
				}	
			}else{
				for($a=1;$a<=12;$a++){
						if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));			
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}	
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$monto = number_format($monto, 2, '.', '');

					$detalle["$a-$i"]["reco"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;
					// echo $monto.PHP_EOL;
					// echo "sss"	;
					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Recoleccion Período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';
					
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="recoleccion" />';
					
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$monto;
					
				}	
			}
			
		}
		//print_r($detalle);
		return $detalle;
	}

	function calculoVariosNuevoDispo($mts, $actividad, $mesInicio, $anoInicio, $mesFin, $anoFin, $dif=0, $fregistro, $actividad2016){
		$ver=[];
		$ver['monto']=0;
		$detalle=[];
		$detalle['monto'] = 0;
		$ver['mensaje'] = '';

		$a2015 = $this->regresarActividadDispo($actividad, 2015, $fregistro, $actividad2016);
		$a2012 = $this->regresarActividadDispo($actividad, 2012, $fregistro, $actividad2016);
		$a2008 = $this->regresarActividadDispo($actividad, 2008, $fregistro, $actividad2016);		
		for($i=$anoInicio;$i<=$anoFin;$i++){		
			if($i<2012){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"]; }	
			if($i>=2012&&$i<2016){$area=$a2012["area"]; $minima=$a2012["minima"]; $tarifa=$a2012["tarifa"]; }
			if($i>2015){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"]; }
			if($i==$anoInicio&&$i!=$anoFin){
				for($a=($mesInicio+1);$a<=12;$a++){
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = $monto*0.10;
					}	
					if($i==2015&$a>11){  
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}	
					$monto = number_format($monto, 2, '.', '');		

					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Disposicion Periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';	
					
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="disposicion" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$monto;
				}
			}elseif($i==$anoFin&&$i!=$anoInicio){
				for($a=1;$a<=$mesFin;$a++){
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
                    $monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = $monto*0.10;
					}	
					if($i==2015&$a>11){  
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}		
					$monto = number_format($monto, 2, '.', '');		
					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Disposicion Periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';	
					
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="disposicion" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto'] = $ver['monto']+$monto;
				}	
			}elseif($i==$anoFin&&$i==$anoInicio){
				for($a=($mesInicio+1);$a<=$mesFin;$a++){
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
                    $monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = $monto*0.10;
					}	
					if($i==2015&$a>11){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}		
					$monto = number_format($monto, 2, '.', '');		
					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;	

					$ver['mensaje']= '<div class="linePer"><div class="linePerText">Disposicion Periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';	
					
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="disposicion" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto'] = $ver['monto']+$monto;
				}	
			}else{
				for($a=1;$a<=12;$a++){
                    if(intval($i.str_pad($a, 2, '0', STR_PAD_LEFT)) >= 201603){
                        $area=$a2015["area"];
						$minima=$a2015["tarifa_rec"] * $this->fa;
						$tarifa=$a2015["tarifa_rec"] * $this->fa;
                    }
                    $monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = $monto*0.10;
					}	
					if($i==2015&&$a>11){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}		
					$monto = number_format($monto, 2, '.', '');	
					$detalle["$a-$i"]["dispo"] = $monto;
					$detalle['monto'] = $detalle['monto']+$monto;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Disposicion Periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.$monto.'</div>';
					
					if($dif==1){
					$ver['mensaje'].='<div class="linePerText"><input class="dif'.$a.$i.'" id="dff" name="diferencia[]" type="text" value="" size="10" placeholder="Cancelado" /></div>';}
					$ver['mensaje'].='</div>';	
					
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.$monto.'" /><input name="tipo[]" type="hidden" value="disposicion" />';
					
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$monto;
				}	
			}
			//print_r($detalle);
		}

		return $detalle;
	}


	function regresarActividad($actividad, $ano, $fechaRegistro=0, $actividad2016){
		//Almacenamos el cambio de codigo de las actividaddes
		include('./connect.php');
		$sql_ac = "SELECT * FROM `2012a2015`";
		$query_ac = $conn->query($sql_ac);

		while($da_ac = $query_ac->fetch_assoc()){
			$arr[$da_ac["codigo"]] = $da_ac["codigo2015"];
			$arr2[$da_ac["codigo2015"]] = $da_ac["codigo"];
		}
		$fe = explode('-',$fechaRegistro);  //ojo en base al fecha registro se busca la informacion
		// print_r($actividad);

		//echo "//// ".$fe[0]." ////";
		$fe[0] = 2015;
			if($fe[0]<=2015){
				if($ano=='2008'){
					$sql="SELECT * FROM `tarifa2008` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2012'){
					$sql="SELECT * FROM `tarifa` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2015'){
					$var = $arr[$actividad];
					if($actividad2016!=""){$var=$actividad2016;}
					$sql="SELECT * FROM `tarifa_2015` WHERE `codigo` = '".$var ."'";
				}
			}
			if($fe[0]==""){  
				if($ano=='2008'){
					$sql="SELECT * FROM `tarifa2008` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2012'){
					$sql="SELECT * FROM `tarifa` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2015'){
					$sql="SELECT * FROM `tarifa_2015` WHERE `codigo` = '".$actividad2016."'";
				}
			}
		// echo $sql;
		// echo "<br>";
		$query = $conn->query($sql);
		$da = $query->fetch_assoc();

		return $da;
	}


	function regresarActividadDispo($actividad, $ano, $fechaRegistro=0, $actividad2016){
		// echo PHP_EOL."kkkkkkkkkkkkkkkkk$actividad, $ano, $fechaRegistro=0, $actividad2016".PHP_EOL;
		include('./connect.php');
		//Almacenamos el cambio de codigo de las actividaddes
		$sql_ac = "SELECT * FROM `2012a2015`";
		$query_ac = $conn->query($sql_ac);

		while($da_ac = $query_ac->fetch_assoc()){
			$arr[$da_ac["codigo"]] = $da_ac["codigo2015"];
			$arr2[$da_ac["codigo2015"]] = $da_ac["codigo"];
		}
		$fe = explode('-',$fechaRegistro);
		//echo $fe[0];
		$fe[0] = 2015;

			if($fe[0]<=2015){
				if($ano=='2008'){
					$sql="SELECT * FROM `tarifa2012_dispo` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2012'){
					$sql="SELECT * FROM `tarifa2012_dispo` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2015'){
					$var = $arr[$actividad];
					if($actividad2016!=""){$var=$actividad2016;}
					$sql="SELECT * FROM `tarifa_2015` WHERE `codigo` = '".$var."'";
				}
			}
			
			if($fe[0]==""){
				if($ano=='2008'){
					$sql="SELECT * FROM `tarifa2012_dispo` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2012'){
					$sql="SELECT * FROM `tarifa2012_dispo` WHERE `codigo` = '".$actividad."'";
				}
				if($ano=='2015'){
					$sql="SELECT * FROM `tarifa_2015` WHERE `codigo` = '".$actividad2016."'";
				}
			
			}
		//echo $sql;
		
		$query = $conn->query($sql);
		$da = $query->fetch_assoc();
			// echo PHP_EOL." fin zzzzzzzz".PHP_EOL;
		return $da;
		// print_r($da);

	}





	function calculoVariosIntereses($mts, $actividad, $mesInicio, $anoInicio, $mesFin, $anoFin, $fregistro, $actividad2016){
		// echo PHP_EOL."xxxwwwww$mts, $actividad, $mesInicio, $anoInicio, $mesFin, $anoFin, $fregistro, $actividad2016".PHP_EOL;

		$ver=[];
		$ver['monto']=0;
		$detalle=[];
		$detalle['monto'] = 0;
		$ver['mensaje'] = '';

		//echo "$actividad, 2015, $fregistro, $actividad2016".PHP_EOL;

		$a2015 = $this->regresarActividad($actividad, 2015, $fregistro, $actividad2016);
		// print_r($a2015);
		$a2012 = $this->regresarActividad($actividad, 2012, $fregistro, $actividad2016);
		// print_r($a2012);
		$a2008 = $this->regresarActividad($actividad, 2008, $fregistro, $actividad2016);
		// print_r($a2008);
		
		
		for($i=$anoInicio;$i<=$anoFin;$i++){		
			if($i<2012){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"]; }	
			if($i>=2012&&$i<2016){$area=$a2012["area"]; $minima=$a2012["minima"]; $tarifa=$a2012["tarifa"]; }
			if($i>2015){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"]; }	
			if($i==$anoInicio&&$i!=$anoFin){
				
				for($a=($mesInicio+1);$a<=12;$a++){		
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}			
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$vi=generarInteresesA($monto,$i,$a);
					$detalle["$a-$i"]["int_reco"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;


					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Recolección período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
				}
				
			}elseif($i==$anoFin&&$i!=$anoInicio){
				for($a=1;$a<=$mesFin;$a++){
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));	
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$vi=generarInteresesA($monto,$i,$a);
					$detalle["$a-$i"]["int_reco"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Recolección período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
					
				}	
			}elseif($i==$anoFin&&$i==$anoInicio){
				for($a=($mesInicio+1);$a<=$mesFin;$a++){
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));	
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$vi=generarInteresesA($monto,$i,$a);	
					$detalle["$a-$i"]["int_reco"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Recolección período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
					
				}	
			}else{
				for($a=1;$a<=12;$a++){
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i==2015&&$a>11){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					if($i>=2016){ $monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i)); }
					$vi=generarInteresesA($monto,$i,$a);	
					$detalle["$a-$i"]["int_reco"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;			
					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Recolección período '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
					
				}	
			}
			
			
		}
		// print_r($detalle);

		return $detalle;
	}



	function calculoVariosInteresesDispo($mts, $actividad, $mesInicio, $anoInicio, $mesFin, $anoFin, $fregistro, $actividad2016){
		// echo PHP_EOL."$mts, $actividad, $mesInicio, $anoInicio, $mesFin, $anoFin, $fregistro".PHP_EOL;
		$ver=[];
		$ver['monto']=0;
		$detalle=[];
		$detalle['monto'] = 0;
		$ver['mensaje'] = '';

		$a2015 = $this->regresarActividadDispo($actividad, 2015, $fregistro, $actividad2016);
		// print_r($a2015);
		$a2012 = $this->regresarActividadDispo($actividad, 2012, $fregistro, $actividad2016);
		// print_r($a2012);
		$a2008 = $this->regresarActividadDispo($actividad, 2008, $fregistro, $actividad2016);
		// print_r($a2008);


		

		for($i=$anoInicio;$i<=$anoFin;$i++){		
			if($i<2012){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"]; }	
			if($i>=2012&&$i<2016){$area=$a2012["area"]; $minima=$a2012["minima"]; $tarifa=$a2012["tarifa"]; }
			if($i>2015){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"]; }	
			if($i==$anoInicio&&$i!=$anoFin){
				
				for($a=($mesInicio+1);$a<=12;$a++){		
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}	
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}		
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}
					if($i==2015&$a>11){  
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}		
					$monto = number_format($monto, 2, '.', '');
					$vi=generarInteresesA($monto,$i,$a);	
					$detalle["$a-$i"]["int_dispo"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Disposicion periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
				}
				
			}elseif($i==$anoFin&&$i!=$anoInicio){
				for($a=1;$a<=$mesFin;$a++){
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;
					}	
					if($i==2015&$a>11){  
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}	
					$monto = number_format($monto, 2, '.', '');
					$vi=generarInteresesA($monto,$i,$a);
					$detalle["$a-$i"]["int_dispo"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Disposicion periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
					
				}	
			}elseif($i==$anoFin&&$i==$anoInicio){
				for($a=($mesInicio+1);$a<=$mesFin;$a++){
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = $monto*0.10;
					}		
					if($i==2015&$a>11){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}
					$monto = number_format($monto, 2, '.', '');
					$vi=generarInteresesA($monto,$i,$a);
					$detalle["$a-$i"]["int_dispo"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Disposicion periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
					
				}	
			}else{
				for($a=1;$a<=12;$a++){
					if($i==2012&&$a<11){$area=$a2008["area"]; $minima=$a2008["minima"]; $tarifa=$a2008["tarifa"];}
					if($i==2015&&$a>11){$area=$a2015["area"]; $minima=$a2015["tarifa_rec"]; $tarifa=$a2015["tarifa_rec"];}
					$monto = $this->basico($mts, $area, $minima, $tarifa, $this->regresaUt($a, $i));	
					if($i>=2016){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = $monto*0.10;
					}
					if($i==2015&$a>11){ 
						$monto = $this->estanco($minima,$mts, $area, $this->regresaUt($a, $i));
						$monto = ($monto/100)*10;					
					}	
					$monto = number_format($monto, 2, '.', '');
					$vi=generarInteresesA($monto,$i,$a);
					$detalle["$a-$i"]["int_dispo"] = $vi;
					$detalle['monto'] = $detalle['monto']+$vi;

					$ver['mensaje'].= '<div class="linePer"><div class="linePerText">Intereses de Disposicion periodo '.$a.'/'.$i.' </div><div class="linePerBs">Bs. '.number_format($vi, 2, ',', '.').'</div></div>';
					$ver['mensaje'].='<input name="periodoFac[]" type="hidden" value="'.$a.'/'.$i.'" /><input name="montoFac[]" type="hidden" value="'.number_format($vi, 2, '.', '').'" /><input name="tipo[]" type="hidden" value="intereses" />';
					$ver['mensaje'].='<div id="linea1"></div>';
					$ver['monto']= $ver['monto']+$vi;
					
				}	
			}
			
		}




		return $detalle;
	}


	


}

?>